#tag DesktopWindow
Begin DesktopWindow MainWindow
   Backdrop        =   0
   BackgroundColor =   &cFFFFFF
   Composite       =   False
   DefaultLocation =   2
   FullScreen      =   False
   HasBackgroundColor=   False
   HasCloseButton  =   True
   HasFullScreenButton=   False
   HasMaximizeButton=   True
   HasMinimizeButton=   True
   Height          =   537
   ImplicitInstance=   True
   MacProcID       =   0
   MaximumHeight   =   32000
   MaximumWidth    =   32000
   MenuBar         =   290306047
   MenuBarVisible  =   False
   MinimumHeight   =   64
   MinimumWidth    =   64
   Resizeable      =   True
   Title           =   "measurements.db Uploader"
   Type            =   0
   Visible         =   True
   Width           =   778
   Begin DesktopLabel Label1
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "measurements.db ファイル"
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   560
   End
   Begin DesktopTextField filepath
      AllowAutoDeactivate=   True
      AllowFocusRing  =   True
      AllowSpellChecking=   False
      AllowTabs       =   False
      BackgroundColor =   &cFFFFFF
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Format          =   ""
      HasBorder       =   True
      Height          =   22
      Hint            =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      MaximumCharactersAllowed=   0
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   52
      Transparent     =   False
      Underline       =   False
      ValidationMask  =   ""
      Visible         =   True
      Width           =   643
   End
   Begin DesktopButton DBFileSelect
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Select..."
      Default         =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   676
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   52
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   83
   End
   Begin DesktopListBox DataView
      AllowAutoDeactivate=   True
      AllowAutoHideScrollbars=   True
      AllowExpandableRows=   False
      AllowFocusRing  =   True
      AllowResizableColumns=   False
      AllowRowDragging=   False
      AllowRowReordering=   False
      Bold            =   False
      ColumnCount     =   7
      ColumnWidths    =   "0,0,50,*"
      DefaultRowHeight=   -1
      DropIndicatorVisible=   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      GridLineStyle   =   0
      HasBorder       =   True
      HasHeader       =   True
      HasHorizontalScrollbar=   False
      HasVerticalScrollbar=   True
      HeadingIndex    =   -1
      Height          =   358
      Index           =   -2147483648
      InitialValue    =   "ID	row_id	Upload	TimeStamp	eNB-LCID	RSRP	TA"
      Italic          =   False
      Left            =   21
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      RowSelectionType=   0
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   127
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   738
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin DesktopButton UploadStartStopBtn
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Upload"
      Default         =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   595
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   497
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   163
   End
   Begin DesktopProgressWheel UploadProgressWheel
      Active          =   False
      AllowAutoDeactivate=   True
      Enabled         =   True
      Height          =   20
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      PanelIndex      =   0
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   86
      Transparent     =   False
      Visible         =   False
      Width           =   25
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopProgressBar UploadProgressBar
      Active          =   False
      AllowAutoDeactivate=   True
      Enabled         =   True
      Height          =   20
      Indeterminate   =   False
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   57
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MaximumValue    =   100
      PanelIndex      =   0
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   86
      Transparent     =   False
      Value           =   50.0
      Visible         =   False
      Width           =   87
      _mIndex         =   0
      _mInitialParent =   ""
      _mName          =   ""
      _mPanelIndex    =   0
   End
   Begin DesktopLabel StateText
      AllowAutoDeactivate=   True
      Bold            =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   156
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0 of 0 / Uploading Cell xxxxxx-yy..."
      TextAlignment   =   0
      TextColor       =   &c000000
      Tooltip         =   ""
      Top             =   86
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   507
   End
   Begin DesktopButton DeselectAllBtn
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Deselect All"
      Default         =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      Italic          =   False
      Left            =   156
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   497
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   124
   End
   Begin DesktopButton SelectAllBtn
      AllowAutoDeactivate=   True
      Bold            =   False
      Cancel          =   False
      Caption         =   "Select All"
      Default         =   False
      Enabled         =   True
      FontName        =   "System"
      FontSize        =   0.0
      FontUnit        =   0
      Height          =   20
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MacButtonStyle  =   0
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Tooltip         =   ""
      Top             =   497
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   124
   End
   Begin Thread TX
      DebugIdentifier =   ""
      Index           =   -2147483648
      LockedInPosition=   False
      Priority        =   5
      Scope           =   0
      StackSize       =   0
      TabPanelIndex   =   0
      ThreadID        =   0
      ThreadState     =   0
   End
   Begin URLConnection ToMLSConnection
      AllowCertificateValidation=   False
      HTTPStatusCode  =   0
      Index           =   -2147483648
      LockedInPosition=   False
      Scope           =   0
      TabPanelIndex   =   0
   End
End
#tag EndDesktopWindow

#tag WindowCode
	#tag Event
		Sub Closing()
		  quit
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events DBFileSelect
	#tag Event
		Sub Pressed()
		  Dim f as FolderItem
		  Dim id as Integer
		  Dim sqlquery as String
		  Dim results as RowSet
		  Dim dlg as new OpenFileDialog
		  Dim dts as DateTime
		  dlg.Title = "measurements.db ファイルを選択"
		  dlg.Filter=AcceptFileTypeGroup.ApplicationDB
		  f=dlg.ShowModal
		  if f<>nil then
		    filepath.Text=f.NativePath
		    DB = New SQLiteDatabase
		    DB.DatabaseFile = f
		    DB.Connect
		    DataView.RemoveAllRows
		    DataView.ColumnTypeAt(2)=DesktopListBox.CellTypes.CheckBox
		    DataView.ColumnAlignmentAt(2)=DesktopListBox.Alignments.Center
		    //sqlquery = "SELECT * FROM cells;"
		    //sqlquery="SELECT cs.row_id, c.cid, cs.rsrp FROM cell_signals cs JOIN cells c ON cs.cell_id = c.row_id JOIN measurements m ON cs.measurement_id = m.row_id;"
		    
		    //sqlquery="SELECT cs.row_id, c.cid, cs.rsrp FROM cell_signals cs JOIN cells c ON cs.cell_id = c.row_id JOIN measurements m ON cs.measurement_id = m.row_id WHERE (cs.cell_id, cs.rsrp) IN ( SELECT cell_id, MAX(rsrp) FROM cell_signals GROUP BY cell_id );"
		    
		    //なんかへん
		    sqlquery = "SELECT cs.*, c.*, m.* FROM ( SELECT *, ROW_NUMBER() OVER (PARTITION BY cid ORDER BY ta ASC, rsrp DESC) AS rn FROM cell_signals JOIN cells ON cell_signals.cell_id = cells.row_id ) cs JOIN cells c ON cs.cell_id = c.row_id JOIN measurements m ON cs.measurement_id = m.row_id WHERE cs.rn = 1 and ta < 99;"
		    
		    //テスト
		    //sqlquery = "SELECT cs.*, c.*, m.* FROM cell_signals cs JOIN cells c ON cs.cell_id = c.row_id JOIN measurements m ON cs.measurement_id = m.row_id WHERE (cs.cell_id, cs.rsrp) IN ( SELECT cell_id, MAX(rsrp) FROM cell_signals GROUP BY cell_id );"
		    
		    Try
		      //results = DB.SelectSQL(sqlquery)
		      //DB.ExecuteSQL(sqlquery,results)
		      results = DB.SelectSQL(sqlquery)
		      
		    Catch e as DatabaseException
		      messagebox e.Message
		    End Try
		    Redim CellLists(results.RowCount, 20)
		    CellCount=results.RowCount
		    id=0
		    For Each row As DatabaseRow in results
		      DataView.Addrow(str(id), row.Column("row_id").StringValue, "", "", "", row.Column("rsrp").StringValue, row.Column("ta").StringValue)
		      id=id+1
		      DataView.CellCheckBoxValueAt(DataView.LastAddedRowIndex, 2)=True
		      dts = New DateTime(row.Column("measured_at").IntegerValue/1000, TimeZone.Current)
		      DataView.CellTextAt(DataView.LastAddedRowIndex, 3)=dts.SQLDateTime
		      DataView.CellTextAt(DataView.LastAddedRowIndex, 4)=Str(floor(row.Column("cid").DoubleValue/256))+"-"+Str(row.Column("cid").DoubleValue Mod 256)
		      
		      //row_id
		      CellLists(DataView.LastAddedRowIndex, 0)=row.Column("row_id").StringValue
		      //timestamp
		      CellLists(DataView.LastAddedRowIndex, 1)=row.Column("measured_at").StringValue
		      //latitude
		      CellLists(DataView.LastAddedRowIndex, 2)=row.Column("lat").StringValue
		      //longitude
		      CellLists(DataView.LastAddedRowIndex, 3)=row.Column("lon").StringValue
		      //accuracy
		      CellLists(DataView.LastAddedRowIndex, 4)=row.Column("accuracy").StringValue
		      //altitude
		      CellLists(DataView.LastAddedRowIndex, 5)=row.Column("altitude").StringValue
		      //heading
		      CellLists(DataView.LastAddedRowIndex, 6)=row.Column("bearing").StringValue
		      //speed (m/s)
		      CellLists(DataView.LastAddedRowIndex, 7)=row.Column("speed").StringValue
		      
		      //source
		      
		      //radioType
		      CellLists(DataView.LastAddedRowIndex, 8)=row.Column("net_type").StringValue
		      //mobileCountryCode
		      CellLists(DataView.LastAddedRowIndex, 9)=row.Column("mcc").StringValue
		      //mobileNetworkCode
		      CellLists(DataView.LastAddedRowIndex, 10)=row.Column("mnc").StringValue
		      //locationAreaCode
		      CellLists(DataView.LastAddedRowIndex, 11)=row.Column("lac").StringValue
		      //cellId
		      CellLists(DataView.LastAddedRowIndex, 12)=row.Column("cid").StringValue
		      //primaryScramblingCode
		      CellLists(DataView.LastAddedRowIndex, 13)=row.Column("psc").StringValue
		      //asu
		      CellLists(DataView.LastAddedRowIndex, 14)=row.Column("asu").StringValue
		      //signalStrength
		      CellLists(DataView.LastAddedRowIndex, 15)=row.Column("dbm").StringValue
		      //timingAdvance
		      CellLists(DataView.LastAddedRowIndex, 16)=row.Column("ta").StringValue
		      //serving
		      //CellLists(DataView.LastAddedRowIndex, 17)=row.Column("neighboring").StringValue
		      CellLists(DataView.LastAddedRowIndex, 17)="1"
		      
		      //ToUpload
		      CellLists(DataView.LastAddedRowIndex, 18)="True"
		    Next
		    
		    StateText.Left=20
		    
		    if results.RowCount > 0 then
		      StateText.Text=str(results.RowCount)+" Cells Available"
		      UploadStartStopBtn.Enabled=True
		    else
		      StateText.Text="NO DATA"
		      UploadStartStopBtn.Enabled=False
		    end if
		    
		    DB.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataView
	#tag Event
		Sub DoublePressed()
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub SelectionChanged()
		  Dim AccessRow As Integer
		  
		  if DataView.SelectedRowIndex <> -1 then
		    AccessRow=Val(Me.CellTextAt(DataView.SelectedRowIndex, 0))
		    
		    //CellInformationWindow.show
		    CellInformationWindow.Title="Cell "+Str(Floor(Val(CellLists(AccessRow, 12))/256))+"-"+Str(Val(CellLists(AccessRow, 12)) Mod 256)+" Properties"
		    CellInformationWindow.LabelTitle.Text="Cell "+Str(Val(CellLists(AccessRow, 12))/256)+"-"+Str(Val(CellLists(AccessRow, 12)) Mod 256)
		    CellInformationWindow.ItemList.RemoveAllRows
		    
		    CellInformationWindow.ItemList.AddRow("Internal ID",Str(AccessRow))
		    CellInformationWindow.ItemList.AddRow("row_id",CellLists(AccessRow, 0))
		    CellInformationWindow.ItemList.AddRow("TimeStamp",CellLists(AccessRow, 1))
		    CellInformationWindow.ItemList.AddRow("latitude",CellLists(AccessRow, 2))
		    CellInformationWindow.latitude=val(CellLists(AccessRow, 2))
		    CellInformationWindow.ItemList.AddRow("longitude",CellLists(AccessRow, 3))
		    CellInformationWindow.longitude=val(CellLists(AccessRow, 3))
		    CellInformationWindow.ItemList.AddRow("accuracy",CellLists(AccessRow, 4))
		    CellInformationWindow.ItemList.AddRow("altitude",CellLists(AccessRow, 5))
		    CellInformationWindow.ItemList.AddRow("heading",CellLists(AccessRow, 6))
		    CellInformationWindow.ItemList.AddRow("speed (m/s)",CellLists(AccessRow, 7))
		    CellInformationWindow.ItemList.AddRow("speed (km/h)",Str(Val(CellLists(AccessRow, 7))*3.6))
		    
		    CellInformationWindow.ItemList.AddRow("radioType",CellLists(AccessRow, 8))
		    CellInformationWindow.ItemList.AddRow("mobileCountryCode",CellLists(AccessRow, 9))
		    CellInformationWindow.ItemList.AddRow("mobileNetworkCode",CellLists(AccessRow, 10))
		    CellInformationWindow.ItemList.AddRow("locationAreaCode",CellLists(AccessRow, 11))
		    CellInformationWindow.ItemList.AddRow("cellId",CellLists(AccessRow, 12))
		    CellInformationWindow.ItemList.AddRow("primaryScramblingCode",CellLists(AccessRow, 13))
		    CellInformationWindow.ItemList.AddRow("asu",CellLists(AccessRow, 14))
		    CellInformationWindow.ItemList.AddRow("signalStrength",CellLists(AccessRow, 15))
		    CellInformationWindow.ItemList.AddRow("timingAdvance",CellLists(AccessRow, 16))
		    CellInformationWindow.ItemList.AddRow("serving",CellLists(AccessRow, 17))
		    CellInformationWindow.ItemList.AddRow("Upload",CellLists(AccessRow, 18))
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub CellAction(row As Integer, column As Integer)
		  if me.CellCheckBoxValueAt(row, 2) = False then
		    msgbox("ID:"+me.CellTextAt(row, 0)+" False "+str(row)+","+str(column))
		    CellLists(Val(me.CellTextAt(row, 0)), 18)="False"
		  else
		    CellLists(Val(me.CellTextAt(row, 0)), 18)="True"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UploadStartStopBtn
	#tag Event
		Sub Pressed()
		  Dim i as integer
		  Dim items() as Dictionary
		  Dim senditems as new Dictionary
		  
		  Dim item,position,cellTower as Dictionary
		  Dim cellTowers() as Dictionary
		  
		  Dim Taskx as new CoreModule.UploaderTask
		  Dim vvv as double
		  
		  for i=0 to DataView.RowCount-1
		    if CellLists(i, 18)="True" then
		      
		      item = new Dictionary
		      
		      item.Value("timestamp") = CellLists(i, 1)
		      item.Value("timestamp") =  item.Value("timestamp").Int64Value
		      
		      position = new Dictionary
		      
		      position.Value("latitude") = CellLists(i, 2)
		      //msgbox(Format(Val(CellLists(i, 2)),"###.00000000"))
		      //position.Value("latitude") = 1
		      position.Value("longitude") = CellLists(i, 3)
		      position.Value("accuracy") = CellLists(i, 4)
		      position.Value("altitude") = CellLists(i, 5)
		      position.Value("heading") = Val(Format(Val(CellLists(i, 6)),"#0"))
		      position.Value("speed") = Val(Format(Val(CellLists(i, 7)),"#0"))
		      position.Value("source") = "gps"
		      
		      cellTower = new Dictionary
		      if CellLists(i, 8) = "4" then
		        cellTower.Value("radioType") = "lte"
		      elseif CellLists(i, 8) = "3" then
		        cellTower.Value("radioType") = "wcdma"
		      end if
		      cellTower.Value("mobileCountryCode") = Integer.FromString(CellLists(i, 9))
		      cellTower.Value("mobileNetworkCode") = Integer.FromString(CellLists(i, 10))
		      cellTower.Value("locationAreaCode") = Integer.FromString(CellLists(i, 11))
		      cellTower.Value("cellId") = Integer.FromString(CellLists(i, 12))
		      cellTower.Value("primaryScramblingCode") = Integer.FromString(CellLists(i, 13))
		      cellTower.Value("asu") = Integer.FromString(CellLists(i, 14))
		      cellTower.Value("signalStrength") = Integer.FromString(CellLists(i, 15))
		      cellTower.Value("timingAdvance") = Integer.FromString(CellLists(i, 16))
		      cellTower.Value("serving") = Integer.FromString(CellLists(i, 17))
		      
		      cellTowers().RemoveAll
		      cellTowers.Add(cellTower)
		      
		      item.Value("position") = position
		      item.Value("cellTowers") = cellTowers
		      items.Add(item)
		    end if
		  Next
		  
		  senditems.Value("items") = items
		  
		  Dim d as new Dictionary
		  d = ParseJSON(jsontext)
		  
		  JSONBodyViewerWindow.show
		  JSONBodyViewerWindow.JSONBodyViewer.Text=""
		  
		  tx.Start
		  
		  JSONBodyViewerWindow.JSONBodyViewer.Text=GenerateJSON(senditems, True)
		  
		  
		  ToMLSConnection.RequestHeader("User-Agent") = "okhttp/4.11.0"
		  ToMLSConnection.SetRequestContent(GenerateJSON(senditems, True), "application/json; charset=utf-8")
		  ToMLSConnection.Send("POST", "https://location.services.mozilla.com/v2/geosubmit")
		  //ToMLSConnection.Send("POST", "https://location.services.mozilla.com/v2/geosubmit?key=8460f950-f3f3-4329-a8a6-ae27cef68ac5")
		  //ToMLSConnection.Send("POST", "https://cbs.brave-hero.net/44011/header-response.php")
		  
		  
		  //ToMLSConnection.Send("POST", "https://location.services.mozilla.com/v2/geosubmit?key=test")
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DeselectAllBtn
	#tag Event
		Sub Pressed()
		  Dim i as Integer
		  For i=0 to DataView.RowCount-1
		    DataView.CellCheckBoxValueAt(i, 2)=False
		    CellLists(Val(DataView.CellTextAt(i, 0)), 18)="False"
		  Next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SelectAllBtn
	#tag Event
		Sub Pressed()
		  Dim i as Integer
		  For i=0 to DataView.RowCount-1
		    DataView.CellCheckBoxValueAt(i, 2)=True
		    CellLists(Val(DataView.CellTextAt(i, 0)), 18)="True"
		  Next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TX
	#tag Event
		Sub Run()
		  Var Dictx as Dictionary
		  Var progressValue As Integer
		  
		  Dim i as integer
		  
		  for i=0 to CellCount
		    Var waitUntil As Integer = System.Ticks + 5
		    
		    While System.Ticks < waitUntil
		    Wend
		    dictx = New Dictionary
		    Dictx.value("sendingcount")=i
		    Dictx.value("objectcount")=CellCount
		    Dictx.value("enb")=Str(Floor(Val(CellLists(i, 12))/256))+"-"+Str(Val(CellLists(i, 12)) Mod 256)
		    
		    if CellLists(i, 18)="True" then
		      Me.AddUserInterfaceUpdate(dictx)
		    end if
		    
		  Next
		  
		  'While progressValue < 100
		  'progressValue = progressValue + 1
		  '
		  '// Do some long-running process
		  'Var waitUntil As Integer = System.Ticks + 15
		  '
		  'While System.Ticks < waitUntil
		  'Wend
		  '
		  '// Call UpdateUI with any parameters you need. This calls the UpdateUI event handler
		  '// where you can directly access any UI controls on the Window.
		  '
		  '// This specifies simple parameters using a Pair
		  '// You can also pass values using a Dictionary
		  '
		  'dictx = New Dictionary
		  'Dictx.value("sendingcount")=i
		  'Dictx.value("objectcount")=DataView.RowCount-1
		  'Dictx.value("enb")=CellLists(i, 12)
		  '
		  'if CellLists(i, 18)="True" then
		  'Me.AddUserInterfaceUpdate(dictx)
		  'end if
		  '
		  '//Me.AddUserInterfaceUpdate("UIProgress":progressValue)
		  '
		  'Wend
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub UserInterfaceUpdate(data() as Dictionary)
		  
		  StateText.Left=156
		  UploadProgressWheel.Visible=True
		  UploadProgressBar.Visible=True
		  
		  //StateText.Left=20
		  For Each arg As Dictionary In data
		    'If arg.HasKey("UIProgress") Then
		    'UploadProgressBar.Value = arg.Value("UIProgress").IntegerValue
		    'End If
		    If arg.HasKey("sendingcount") Then
		      UploadProgressBar.Value = arg.Value("sendingcount").IntegerValue
		    End If
		    If arg.HasKey("objectcount") Then
		      UploadProgressBar.MaximumValue = arg.Value("objectcount").IntegerValue
		    End If
		    If arg.HasKey("enb") Then
		      StateText.Text = Str(arg.Value("sendingcount").IntegerValue)+" of "+Str(arg.Value("objectcount").IntegerValue)+" / Uploading Cell "+arg.Value("enb").StringValue+"..."
		    End If
		  Next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ToMLSConnection
	#tag Event
		Sub ContentReceived(URL As String, HTTPStatus As Integer, content As String)
		  msgbox(content)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		InitialValue=""
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimumWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimumHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximumWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximumHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Type"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Types"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasCloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasMaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasMinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasFullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=false
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=false
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=false
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="DefaultLocation"
		Visible=true
		Group="Behavior"
		InitialValue="2"
		Type="Locations"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Windows Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackgroundColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackgroundColor"
		Visible=true
		Group="Background"
		InitialValue="&cFFFFFF"
		Type="ColorGroup"
		EditorType="ColorGroup"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		InitialValue=""
		Type="Picture"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		InitialValue=""
		Type="DesktopMenuBar"
		EditorType=""
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Deprecated"
		InitialValue="False"
		Type="Boolean"
		EditorType=""
	#tag EndViewProperty
#tag EndViewBehavior
