<?php
if (!empty($_GET["userid"])) {
	if (empty($_COOKIE['loginid'])) {
		setcookie("loginid", $_GET["userid"], time()+3600*24*365);
		header("Refresh:0");
		return('');
	}
}
?>
<!DOCTYPE html>
<html lang="ja">
    <HEAD>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,user-scalable=no">
	<meta name="author" content="isogame">
	<title>MLSデータアップロードマネージャー</title>
	<meta name="apple-mobile-web-app-title" content="MLS Manager">
	<link rel="stylesheet" href="mlsuploadmanager_core.css">
	<link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
	<link rel="apple-touch-icon" href="https://44011.brave-hero.net/apple-touch-icon.png">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin=""/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="mlsuploadmanager_core.js?20240229"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css">
	<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>-->
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>-->
	<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
	<!--<script src="jquery.ui.touch-punch.min.js"></script>
	<script src="Highcharts/code/highcharts.js" async></script>
	<script src="jslib/html2canvas.min.js" async></script>
	<script src="konami.js"></script>-->
    </HEAD>
    <BODY>
	<script>
		window.onload = function() {
			getglobalhome();
			getpersonalize();
			//keyword_save = "~^|"; //dummy
			xmlhttp = null;
			//baseTime = new Date();
			//konami_success = "";
			//query('');
			//var q = document.getElementById("q");
			//if (q.addEventListener) {
			//	q.addEventListener("keyup", query, false) //Firefox, Opera, Safari
			//} else {
			//	q.attachEvent("onkeyup", query); //IE
			//}
			//setInterval("query('1')", 1000);
			//getscoretoolhome();
			//konami = new Konami()
			//konami.load()
		}
	</script>
        	<!-- バージョン表示（画面右上） -->
	<div class="yubinavi_version">SEASON2:PRODUCTION:20240229:01</div>
        	<!-- プロダクションバナーと上部の3メインタブメニュー描画 -->
	<div class="yubinavi_production_banner" id="yubinavi_production_banner">
    <div style="display: inline-block; line-height: 50px; font-size: clamp(0.8rem, calc(0.25vw + 1.05rem), 1.8rem);">
			<h3>MLSデータアップロードマネージャー</h3><!--
			<div style="font-size: 10px;">
				<h3>SEASON 9.0: Passion</h3>
			</div>-->
		</div>
    <div style="font-size: 14px; position: relative; bottom: 4px; overflow: hidden; word-break: keep-all; left: -1px;">
	<?php
		if (empty($_COOKIE['loginid'])) {
			echo "ログインしていません";
		} else {
			echo "ログイン中";
		}
	?></div>
    		<!-- 3メインタブ -->
			<?php
		if (!empty($_COOKIE['loginid'])) {
		echo '<div style="font-size: 16px; overflow: hidden; height: 31px;">';
		echo '<div class="tabswitch" style="height: 30px; border-radius: 6px 6px 0px 0px;" id="home-tab" onclick="ChangeTab(\'home-tab\');">ホーム</div>';
			echo '<div class="tabswitch" id="search-tab" onclick="ChangeTab(\'search-tab\');">パーソナライズ</div>';
			echo '</div>';
		}
	?>
		<!-- ここまで3メインタブ -->
</div>
	<!-- ここまでプロダクションバナー部 -->

    	<!-- ホームタブ -->
	<div id="home-tab-box" class="tabbox">
		<!-- グローバルホーム -->
		<div id="global_home" style="overflow: hidden; margin-top: 5px;">
			<div style="text-align: center; margin: 10px;"><i class="fas fa-sync-alt fa-spin fa-xs" style="color: darkcyan"></i> please wait, Loading Data...</div>
		</div>


	</div>

	<!-- 楽曲検索タブ -->
	<div id="search-tab-box" class="tabbox" style="display: none;">


	<div id="personalize_settings" style="overflow: hidden; margin-top: 5px;">
			<div style="text-align: center; margin: 10px;"><i class="fas fa-sync-alt fa-spin fa-xs" style="color: darkcyan"></i> please wait, Loading Personalize Settings...</div>
		</div>

    </div>



</BODY>
</HTML>