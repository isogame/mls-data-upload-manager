var personalize_loaded = 0;
var map;
var geofencinglastmarker;
var geofencinglastround;

var geofencingmarkers = [];


// jQuery Functions
$(function () {
	// ページの上の方にいる間は「ページの一番上に戻る」を表示しない
	var ToPageTop = $('#page-top');
	//ToPageTop.hide();
	ToPageTop.css('bottom', '-100px');
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			//ToPageTop.fadeIn();
			ToPageTop.stop().animate({ 'bottom': '0px' }, 100);
		} else {
			//ToPageTop.fadeOut();
			ToPageTop.stop().animate({ 'bottom': '-40px' }, 100);
		}
	});
	// ページの一番上に戻る
	ToPageTop.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});

});

//画面上部のタブ制御
function ChangeTab(tabname) {
	document.getElementById('home-tab-box').style.display = 'none';
	document.getElementById('search-tab-box').style.display = 'none';

	document.getElementById('home-tab').style.height = 'auto';
	document.getElementById('search-tab').style.height = 'auto';

	document.getElementById('home-tab').style.borderRadius = '6px';
	document.getElementById('search-tab').style.borderRadius = '6px';


	if (tabname == "enquete-tab") {
		enquete_load();
	}

	document.getElementById(tabname + "-box").style.display = 'block';
	document.getElementById(tabname).style.height = '30px';
	document.getElementById(tabname).style.borderRadius = '6px 6px 0px 0px';


	if (tabname == "search-tab") {
		if (personalize_loaded == '0') {
			alert('パーソナライズ設定が読み込まれていません');
		}
		map.invalidateSize()
	}
}

function AddGeofencing(marker) {
	//if ( geofencinglastmarker != null ) { console.log(""); map.removeLayer(geofencinglastmarker); map.removeLayer(geofencinglastround); };
	//geofencinglastmarker = 
	//geofencinglastround = L.circle(marker.latlng, { radius: 300, color: "navy", fill: true, wegiht: 2 }).addTo(map);
	geofencingmarkers.push(L.marker(marker.latlng).addTo(map).on('click',function(e) { RemoveGeofencing(e); }));
	var radius = document.getElementById('geofencing-radius').value;
	if (radius == "") { radius = "1200"; document.getElementById('geofencing-radius').value = "1200"; }
	geofencingmarkers[geofencingmarkers.length-1].radiusobj = L.circle(marker.latlng, { radius: radius, color: "navy", fill: true, wegiht: 2 }).addTo(map);
	geofencingmarkers[geofencingmarkers.length-1].lat = marker.latlng.lat;
	geofencingmarkers[geofencingmarkers.length-1].lng = marker.latlng.lng;
	geofencingmarkers[geofencingmarkers.length-1].radius = radius;

	setUserSettings('add_geofencing','',marker.latlng.lat+','+marker.latlng.lng+','+radius,'geofencingstatus');

	console.log("Geofencing Add: "+marker.latlng.lat+','+marker.latlng.lng+','+radius);
}

function RemoveGeofencing(e) {
	console.log("Geofencing Remove: "+e.target.lat+","+e.target.lng+" Radius: "+e.target.radius);
	setUserSettings('remove_geofencing','',e.target.lat+','+e.target.lng+','+e.target.radius,'geofencingstatus');
	map.removeLayer(e.target.radiusobj);
	map.removeLayer(e.target);
}

function LoadGeofencing() {
	var geofencinglist_csv = document.getElementById("geofencinglist_csv").innerText;
	//console.log("Array Length: "+geofencinglist_array.length);
	console.log("geofencinglist_csv: "+geofencinglist_csv);
	if (geofencinglist_csv.length > 0) {
		var rows = geofencinglist_csv.trim().split('\n');
		rows.forEach(function(row) {
			var columns = row.split(',');
			var lat = parseFloat(columns[0]);
			var lng = parseFloat(columns[1]);
			var radius = parseFloat(columns[2]);
			console.log("Drawing: "+lat+","+lng+" Radius:"+radius+"m...");
			geofencingmarkers.push(L.marker([lat, lng]).addTo(map).on('click',function(e) { RemoveGeofencing(e); }));
			//L.circle([lat,lng], { radius: radius, color: "navy", fill: true, wegiht: 2 }).addTo(map);
			geofencingmarkers[geofencingmarkers.length-1].radiusobj = L.circle([lat,lng], { radius: radius, color: "navy", fill: true, wegiht: 2 }).addTo(map);
			geofencingmarkers[geofencingmarkers.length-1].lat = lat;
			geofencingmarkers[geofencingmarkers.length-1].lng = lng;
			geofencingmarkers[geofencingmarkers.length-1].radius = radius;
		});
	}
    // 各行を多次元配列に格納する
    //var geofencinglist_array = rows.map(function(row) {
    //    return row.split(',');
    //});
    //console.log(geofencinglist_array);
}

function LoadExcludeENBList() {
	var excludeenblist_csv = "";
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("GET", "load_excludeenblcid.php", true);
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			excludeenblist_csv = ajaxobj.responseText;

			console.log("excludeenblist_csv: "+excludeenblist_csv);
			
			//Geofencingと同じようなことをやっているはずなのに、こちらはデータがないときに1バイト何かが返ってくる。謎。
			if (excludeenblist_csv.length > 1) {
				excludeenblist_html = "";
				excludeenblist_html += '<div style="height: auto;" class="personalizesettings_excludeenblcid_tableall">';
				excludeenblist_html += '<div class="personalizesettings_excludeenblcid_celldelbtn" style="border-width: 3px 3px 0px 3px;">削除</div>';
				excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellmcc" style="border-width: 3px 3px 0px 0px;">MCC</div>';
				excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellmnc" style="border-width: 3px 3px 0px 0px;">MNC</div>';
				excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellcellid" style="border-width: 3px 3px 0px 0px;">eNB-LCID</div>';
				excludeenblist_html += '</div>';
				var rows = excludeenblist_csv.trim().split('\n');
				rows.forEach(function(row) {
					var columns = row.split(',');
					var mcc = parseFloat(columns[0]);
					var mnc = parseFloat(columns[1]);
					var enblcid = columns[2];
					console.log("Show: MCC "+mcc+" MNC "+mnc+" eNB-LCID:"+enblcid+"...");
					excludeenblist_html += '<div style="height: auto;" class="personalizesettings_excludeenblcid_tableall">';
					excludeenblist_html += '<div class="personalizesettings_excludeenblcid_celldelbtn"><input type="button" value="削除" onclick="setUserSettings(\'remove_excludeenb\',\'\',\''+mcc+','+mnc+','+enblcid+'\',\'excludeenbstatus\'); LoadExcludeENBList();"></div>';
					excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellmcc">'+mcc+'</div>';
					excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellmnc">'+mnc+'</div>';
					excludeenblist_html += '<div class="personalizesettings_excludeenblcid_cellcellid">'+enblcid+'</div>';
					excludeenblist_html += '</div>';
				});
			} else {
				excludeenblist_html = "1件も登録されていません";
			}
			document.getElementById("excludeenblcidlist").innerHTML = excludeenblist_html;
		}
	}
	ajaxobj.send(null)
}

function createXmlHttpRequest() {
	if (window.XMLHttpRequest) {                       // Firefox,Opera,Safari,IE7
		return new XMLHttpRequest();
	} else if (window.ActiveXObject) {                 // IE5,IE6
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");    // MSXML3
		} catch (e) {
			return new ActiveXObject("Microsoft.XMLHTTP"); // MSXML2まで
		}
	} else {
		return null;
	}
}

//グローバルホームを表示
function getglobalhome() {
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("GET", "uploaddata_list.php", true);
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			document.getElementById("global_home").innerHTML = ajaxobj.responseText;
			console.log('Personalize Settings Loaded.');
			
			window.setTimeout(function(){
				LoadExcludeENBList();
				map = L.map('geofencing-map').setView([35.246112654438576, 136.7769245612252], 6);
				L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
					maxZoom: 19,
					attribution: ' &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
				}).addTo(map);
				//map.invalidateSize()
				map.on('click', AddGeofencing)
				LoadGeofencing();
			}, 2000);

		}
	}
	ajaxobj.send(null)
}

//パーソナライズタブを表示
function getpersonalize() {
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("GET", "load_personalize.php", true);
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			document.getElementById("personalize_settings").innerHTML = ajaxobj.responseText;

			personalize_loaded = '1';
		}
	}
	ajaxobj.send(null)
}

//IDの作成
function createid() {
	if (!document.getElementById("agreeprivacy").checked || !document.getElementById("agreeterms").checked) {
		document.getElementById("createidresult").textContent = "必読の項目が確認されていません";
		return;
	}
	if (document.getElementById("createnickname").value == "") {
		document.getElementById("createidresult").textContent = "ニックネームが入力されていません";
		return;
	}
	document.getElementById("createidresult").textContent = "作成しています";
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("POST", "idcreate.php");
	ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajaxobj.send("nickname="+encodeURIComponent(document.getElementById("createnickname").value));
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			console.log("ID Create Response: "+ajaxobj.responseText);
			responsearray = ajaxobj.responseText.split(';');
			if (responsearray[0] == "success") {
				document.getElementById("createidresult").innerHTML = "IDを作成しました。<BR>1.再度ログインする場合に必要ですので、必ずメモってください。忘れた場合はもう一度作成しなおしてください。<BR>2.Tower Collectorの「：」→「Preferences」→「Upload」→「Use custom MLS service」をオンにしたあと「Custom MLS service address」をタッチし、下の「アップロード用URL」を貼り付けて「OK」してください。<BR>3.このページをリロードすると「MLSデータアップロードマネージャー」がログインされた状態で表示されます。";
				document.getElementById("createdid").innerHTML = responsearray[1];
				document.getElementById("createduploadurl").innerHTML = 'https://44011.brave-hero.net/api/v1/geosubmit?key=' + responsearray[1];
				document.getElementById("creatediddetailblock").style.display = "block";
				document.getElementById("createform").style.display = "none";
				setCookie('loginid',responsearray[1],365);
			} else {
				document.getElementById("createidresult").innerHTML = "IDの作成に失敗しました";
			}
			
		}
	}
	//ajaxobj.send(null)
}

//作成したIDをクリップボードにコピーする
function createdidcopy() {
	navigator.clipboard.writeText(document.getElementById('createdid').textContent).then (
		() => {
			alert(document.getElementById('createdid').textContent+' をコピーしました！\nMLS Upload Helperの「MLSデータアップロードマネージャーのID」に貼り付けて「保存」してください。');
		},
		() => {
			alert('コピーできませんでした。。。');
		});
}

//作成したIDをクリップボードにコピーする
function createduploadurlcopy() {
	navigator.clipboard.writeText(document.getElementById('createduploadurl').textContent).then (
		() => {
			alert(document.getElementById('createduploadurl').textContent+' をコピーしました！\nTower Collectorの「Custom MLS service URL」に貼り付けて「OK」してください。');
		},
		() => {
			alert('コピーできませんでした。。。');
		});
}

//IDの削除
function deleteid() {
	if (!document.getElementById('deletechk').checked) {
		alert("「本当に削除する」にチェックが入っていません");
		return;
	}
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("POST", "iddelete.php");
	ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajaxobj.send("delete="+encodeURIComponent("ok"));
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			console.log("ID Delete Response: "+ajaxobj.responseText);
			responsearray = ajaxobj.responseText.split(';');
			if (responsearray[0] == "success") {
				setCookie('loginid',"",-1);
				alert("MLSデータアップロードマネージャーIDを削除しました。ご利用ありがとうございました");
				location.reload();
			} else {
				alert("IDの削除に失敗しました");
			}
			
		}
	}
	//ajaxobj.send(null)
}

//アップロードのキャンセルを送信
function UploadCancel(itemid,cellid,sendcount) {
	var sendconfirm = "";
	//sendcountは現状SELECTできていないのでこれは無意味
	if (sendcount > 0) { sendconfirm = "データが1度以上送信されているため、キャンセルしてもMLSに反映される可能性があります。"; } else { sendconfirm = "まだMLSに初回送信されていないため、ここでキャンセルすれば反映されないと思われます。"; }
	
	if ( confirm("このeNB-LCID "+Math.floor(cellid/256)+"-"+cellid%256+" のアップロードをキャンセルしてもよろしいですか？") == true ) {
		document.getElementById(itemid).textContent = "キャンセルしています...";
		var ajaxobj = null;
		if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
		if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
			return;
		}
		ajaxobj.open("POST", "cancelrequest.php");
		ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		ajaxobj.send("mode=stop&cellid="+encodeURIComponent(cellid));
		ajaxobj.onreadystatechange = function () {
			if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
				console.log("Cancel Upload eNB-LCID: "+ajaxobj.responseText);
				responsearray = ajaxobj.responseText.split(';');
				if (responsearray[0] == "success") {
					document.getElementById(itemid).innerHTML = "アップロードをキャンセルしました";
				} else {
					document.getElementById(itemid).innerHTML = "アップロードのキャンセルに失敗しました";
				}
				
			}
		}
		//ajaxobj.send(null)
	}
}

//ニックネームの変更
function setNickname(nickname) {
	if (nickname == "") {
		document.getElementById("nicknamestatus").textContent = "ニックネームが入力されていません";
		return;
	}
	document.getElementById("nicknamestatus").textContent = "変更しています...";
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("POST", "changeparam.php");
	ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajaxobj.send("mode=change_nickname&nickname="+encodeURIComponent(nickname));
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			console.log("Change Nickname Response: "+ajaxobj.responseText);
			responsearray = ajaxobj.responseText.split(';');
			if (responsearray[0] == "success") {
				document.getElementById("nicknamestatus").innerHTML = "ニックネームを変更しました";
			} else {
				document.getElementById("createidresult").innerHTML = "ニックネームの変更に失敗しました";
			}
			
		}
	}
	//ajaxobj.send(null)
}

//設定値の送信
function setUserSettings(mode,datatype,value,returndiv) {
	if (value === "") {
		document.getElementById(returndiv).textContent = "設定値を入力してください";
		return;
	}
	document.getElementById(returndiv).textContent = "変更しています...";
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("POST", "changeparam.php");
	ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajaxobj.send("mode="+mode+"&datatype="+encodeURIComponent(datatype)+"&value="+encodeURIComponent(value));
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			console.log("Set User Settings Response: "+ajaxobj.responseText);
			responsearray = ajaxobj.responseText.split(';');
			if (responsearray[0] == "success") {
				document.getElementById(returndiv).innerHTML = responsearray[1];
			} else {
				document.getElementById(returndiv).innerHTML = responsearray[1];
			}
			
		}
	}
	//ajaxobj.send(null)
}

function checkCellIDFormat(MCC, MNC, eNBLCID) {
	const checks = {
	  MCCCheck: /^(440|441)$/.test(MCC),
	  MNCCheck: /^\d{2,3}$/.test(MNC),
	  ENBLCIDCheck: /^\d{6}-\d{1,2}$/.test(eNBLCID)
	};
  
	const failedChecks = Object.entries(checks).filter(([key, value]) => !value).map(([key]) => key);
  
	if (failedChecks.length === 0) {
		setUserSettings('add_excludeenb','',MCC+','+MNC+','+eNBLCID,'excludeenbstatus');
		LoadExcludeENBList();
	} else {
	  // エラーメッセージを生成
	  let errorMessage = "エラー: ";
	  if (failedChecks.includes("MCCCheck")) {
		errorMessage += "MNCは440または441である必要があります。";
	  }
	  if (failedChecks.includes("MNCCheck")) {
		errorMessage += "MCCは2桁または3桁の数字である必要があります。";
	  }
	  if (failedChecks.includes("ENBLCIDCheck")) {
		errorMessage += "eNB-LCIDは6桁の数字とハイフン、その後に1桁または2桁の数字である必要があります。";
	  }
  
	  document.getElementById('excludeenbstatus').textContent = errorMessage;
	}
  }

  
//除外eNB-LCIDの設定
function setExcludeeNBLCIDSettings(mode,mcc,mnc,cellid) {
	if (value == "") {
		document.getElementById(returndiv).textContent = "設定値を入力してください";
		return;
	}
	document.getElementById(returndiv).textContent = "変更しています...";
	var ajaxobj = null;
	if (!ajaxobj) ajaxobj = createXmlHttpRequest(); //XMLHttpRequestを生成する
	if (!ajaxobj || ajaxobj.readyState == 1 || ajaxobj.readyState == 2 || ajaxobj.readyState == 3) {
		return;
	}
	ajaxobj.open("POST", "changeparam.php");
	ajaxobj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajaxobj.send("mode="+mode+"&datatype="+encodeURIComponent(datatype)+"&value="+encodeURIComponent(value));
	ajaxobj.onreadystatechange = function () {
		if (ajaxobj.readyState == 4 && ajaxobj.status == 200) {
			console.log("Set User Settings Response: "+ajaxobj.responseText);
			responsearray = ajaxobj.responseText.split(';');
			if (responsearray[0] == "success") {
				document.getElementById(returndiv).innerHTML = "設定を変更しました";
			} else {
				document.getElementById(returndiv).innerHTML = "設定に失敗しました";
			}
			
		}
	}
	//ajaxobj.send(null)
}

// クッキー保存　setCookie(クッキー名, クッキーの値, クッキーの有効日数 日指定); //
function setCookie(c_name,value,expiredays){
    // pathの指定
    var path = location.pathname;
    // pathをフォルダ毎に指定する場合のIE対策
    var paths = new Array();
    paths = path.split("/");
    if(paths[paths.length-1] != ""){
        paths[paths.length-1] = "";
        path = paths.join("/");
    }
	var path = "/";
    // 有効期限の日付
    var extime = new Date().getTime();
    var cltime = new Date(extime + (60*60*24*1000*expiredays));
    var exdate = cltime.toUTCString();
    // クッキーに保存する文字列を生成
    var s="";
    s += c_name +"="+ escape(value);// 値はエンコードしておく
    s += "; path="+ path;
    if(expiredays){
        s += "; expires=" +exdate+"; ";
    }else{
        s += "; ";
    }
    // クッキーに保存
    document.cookie=s;
}

// クッキーの値を取得 getCookie(クッキー名); //
function getCookie(c_name){
    var st="";
    var ed="";
    if(document.cookie.length>0){
        // クッキーの値を取り出す
        st=document.cookie.indexOf(c_name + "=");
        if(st!=-1){
            st=st+c_name.length+1;
            ed=document.cookie.indexOf(";",st);
            if(ed==-1) ed=document.cookie.length;
            // 値をデコードして返す
            return unescape(document.cookie.substring(st,ed));
        }
    }
    return "";
}

// クッキーの値をアラートで表示
//function checkCookie(){
//    if(getCookie('testName')){
//        var setName = getCookie('testName');
//        alert(setName);
//    }
//}