<?php
//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

$loginid=escs($db_session,$_COOKIE["loginid"]);

if ($loginid == "") {
    exit;
}


// トランザクション開始
$db_session->begin_transaction();

try {
    $userid_to_delete = $loginid;

    // queue_dataから該当するuploadidのデータを削除する準備
    $stmt1 = $db_session->prepare("DELETE FROM queue_data WHERE uploadid IN (SELECT uploadid FROM queue_list WHERE userid = ?)");
    $stmt1->bind_param('s', $userid_to_delete);
    $stmt1->execute();
    $stmt1->close();

    // queue_listからuseridに該当するレコードを削除する準備
    $stmt2 = $db_session->prepare("DELETE FROM queue_list WHERE userid = ?");
    $stmt2->bind_param('s', $userid_to_delete);
    $stmt2->execute();
    $stmt2->close();

    // user_execludeenblistからuseridに該当するレコードを削除する準備
    $stmt3 = $db_session->prepare("DELETE FROM user_execludeenblist WHERE userid = ?");
    $stmt3->bind_param('s', $userid_to_delete);
    $stmt3->execute();
    $stmt3->close();

    // user_geofencinglistからuseridに該当するレコードを削除する準備
    $stmt4 = $db_session->prepare("DELETE FROM user_geofencinglist WHERE userid = ?");
    $stmt4->bind_param('s', $userid_to_delete);
    $stmt4->execute();
    $stmt4->close();

    // user_listからuseridに該当するレコードを削除する準備
    $stmt5 = $db_session->prepare("DELETE FROM user_list WHERE userid = ?");
    $stmt5->bind_param('s', $userid_to_delete);
    $stmt5->execute();
    $stmt5->close();

    // トランザクションをコミット
    $db_session->commit();
} catch (mysqli_sql_exception $exception) {
    // エラーがあった場合はロールバック
    $db_session->rollback();
    throw $exception;
}

echo "success;";
//MySQL接続解除
dbdisconnect($db_session);
?>