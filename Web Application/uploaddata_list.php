<?php
//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

if ($_COOKIE['loginid'] == "") {
    echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> MLSデータアップロードマネージャーIDを入力すると、アップロードデータの管理を行うことができます。';
    echo '<form name="loginbox" style="font-size: 12px; padding: 4px;">';
    echo 'ID <input type="text" name="loginid" value="" size="35" placeholder="XM117e24f3...のような形式のテキスト"> ';
    echo '<input type="button" value="ログイン" onclick="setCookie(\'loginid\',loginbox.loginid.value,365); location.reload();"></form>';
    echo '</div>';
    $loginid = "";
    //$loginid = "Anonymous";
} else {
    $sqlquery = "SELECT userid,nickname from user_list where userid = '".escs($db_session,$_COOKIE['loginid'])."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
        if ($database_response["nickname"] !== '') {
            $loginname = $database_response["nickname"];
        } else {
            $loginname = "ニックネームが未設定";
            //$loginname = $_COOKIE['loginid'];
        }
        //echo '<div style="font-size: 28px; background-color: white; background: linear-gradient(white 15.213333333333%, #34e981 110%);">1271<span style="font-size: 18px">.8</span></div>';
        echo '<div class="eventtext" style="background-color: pink; background: linear-gradient(white 15.213333333333%, #34e981 110%); color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> '.$loginname.'さんがアップロードしたデータを表示しています。(<a onclick="setCookie(\'loginid\',\'\',365); location.reload();">ログアウト</a>)</div>';
        $loginid = escs($db_session,$_COOKIE['loginid']);
    } else {
        echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> 入力したID '.$_COOKIE['loginid'].' でユーザーデータが見つかりませんでした。';
        echo '<form name="loginbox" style="font-size: 12px; padding: 4px;">';
        echo 'ID <input type="text" name="loginid" value="" size="35"> ';
        echo '<input type="button" value="ログイン" onclick="setCookie(\'loginid\',loginbox.loginid.value,365); location.reload();"></form>';
        echo '</div>';
        //なぜかできない→setcookie("loginid", "", time() - 3600);
        $loginid = "";
    }
}

echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> 2024/02/29 Update: 外観が変わりました。Tower Collector からのダイレクトアップロード用APIを公開しました。</div>';

if ($loginid == "") {
    echo '<div class="eventtext" style="background-color: lightgray; color: #000000;">';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> MLS Upload Helper と MLSデータアップロードマネージャーについて<BR>';
    echo '"MLSデータアップロードマネージャー"は、Tower Collector で収集したデータを Mozilla Location Services (MLS) に電測データを一発でアップロードするために必要なツールを提供します。<BR>';
    echo '2023年現在、Mozilla Location Services は何故か調子が悪く、Tower Collector で収集したデータを MLS に送信したときに、かなり高い確率でデータをロストしてしまうことが知られています。<BR>';
    echo 'この問題について、データが反映されるまで定期的に再アップロードしなければならない状態が1年以上続いており、改善するのかどうかわからない状態です。<BR>';
    echo 'そこで、Tower Collector から　MLS互換のJSONファイルをエクスポートし、"MLS Upload Helper"というアプリケーション経由で、"MLSデータアップロードマネージャー"へデータをいったん送信し<BR>';
    echo 'MLSデータアップロードマネージャーからMLSへデータが反映されるまで再アップロードを繰り返すという仕組みを考案しました。MLSの調子がよくなればこのようなシステムはお払い箱ですが、電測活動の一助になればとおもいます。</p>';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> セットアップ方法<BR>';
    echo '1. 以下の文章をよく読んで、ユーザーIDの登録を行って「XG・・・・・・」の文字列（MLSデータアップロードマネージャーID）を入手してください。<BR>';
    echo '2. "MLS Upload Helper" のAPKファイルを以下のリンクから入手して Android 端末にインストールしてください。<BR>';
    echo '<A href="https://gitlab.com/isogame/mls-data-upload-manager/-/raw/main/Client%20Applications%20(Android%20MLS%20Upload%20Helper)/app/release/MLS-Upload-Helper-1-2-public-beta-release.apk">MLS-Upload-Helper-1-2-public-beta-release.apk</a><BR>';
    echo '3. "MLS Upload Helper" を起動して "MLSデータアップロードマネージャーID" を入力して「保存」をタッチします。<BR>';
    echo 'Im so Happyのスイッチは特に意味はないです。<BR>';
    echo '4. Tower Collectorで電測したあと、右上のハンバーガーメニュー「：」から「Export」→「JSON for Mozilla Location Services」にチェック→「EXPORTボタン」→「Share」→「SHAREボタン」→「MLS Upload Helper」に共有してください。<BR>';
    echo '5. このWebサイト「MLSデータアップロードマネージャー」にアップロードの状況が表示されます。<BR>';
    echo 'なお、MLSデータアップロードマネージャーではTower Collectorで電測したセルをeNB-LCID単位で、あるいは指定した場所付近のセルをまとめてアップロードしないように設定することもできます。「パーソナライズ」タブをご覧ください。<BR>';

    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> あなたのプライバシーについて（要熟読）<BR>';
    echo 'MLSデータアップロードマネージャーの仕組みの項で説明しますが、MLS Upload HelperにTower Collectorからエクスポートしたデータを共有することで、データがこのサイトの管理人から見える場所に渡ってしまうことに注意してください。<BR>';
    echo 'これには Tower Collector で基地局データをマッピングしている間のすべての時刻データ、その時刻に受信していた基地局のセル情報、高精度GPS位置情報データが含まれます。<BR>';
    echo 'また、データ最適化（アップロードが完了したeNB-LCIDに関するデータを部分的に削除したり、データ軽量化のために同じような電測データをアップロード除外するなど）を行います。<BR>';
    echo 'この点に同意できない場合は、このシステムを利用することができませんので、ご了承ください（仕組み上仕方ないことです）<BR>';
    echo 'また、このサイトで発行する「MLSデータアップロードマネージャーID」は他人に知られないようにしてください。知られると、あなたの電測データを閲覧される可能性があります。<BR>';
    echo '<input type="checkbox" id="agreeprivacy"> 確認しました</p>';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> MLSデータアップロードマネージャーの信頼性について（要熟読）<BR>';
    echo 'MLSデータアップロードマネージャーの不具合や何らかのトラブルにより、あなたの貴重な電測データをロストしてしまうことがあるかもしれません。<BR>';
    echo '電測データは必ずご自身でバックアップしてください。電測データの損失や、何らかの損害が生じたとしても、管理人は一切責任を負わないものとします。<BR>';
    echo '<input type="checkbox" id="agreeterms"> 確認しました</p>';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> MLS Upload Helper と MLSデータアップロードマネージャーの仕組みについて<BR>';
    echo 'MLS Upload Helper は、Tower Collector にもともと存在するエクスポート機能を使って書き出された「Mozilla Location ServicesのAPI仕様に適合したJSON形式のデータ」を読み取ることができます。<BR>';
    echo 'これをMLSデータアップロードマネージャーへアップロードします。MLSデータアップロードマネージャーは、受け取った情報をMLSのgeosubmit APIに送信します。<BR>';
    echo 'その後、1時間おきに更新されるMLSの差分更新データをチェックして、アップロードしたeNB-LCIDが出現したら、そのeNB-LCIDについて再送信しないようになります。出現しない場合は、再度アップロードを行います。<BR>';
    echo '（※この仕様については今後、MLSにとってもっと負荷が軽減されるような再送信方法に変更され、今より反映までに時間がかかるようになる可能性があります）<BR>';
    echo 'MLSの差分更新データに、あなたの送信したeNB-LCIDが出現した段階で再送信を止めてしまいます。これはあなたが測定した電測データではないかもしれないという点に注意してください。<BR>';
    echo '（あなたが、該当の基地局のそばで正確な位置情報をもって電測していたとしても、別の人が該当の基地局から遠く離れた場所で観測したデータを、あなたと同時刻に送信していた場合に<BR>';
    echo '　そちらのデータがMLSに反映されたことをもって、MLSデータアップロードマネージャーがデータの再送信を止めてしまうことで、あなたの電測データが反映されないかもしれないということです）</p>';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> MLSデータアップロードマネージャーは誰が運営していますか？<BR>';
    echo 'ドクオ(X: @isogame)が運営しています。Eメールでの連絡先は <img src="mail.png"> です。</p>';
    echo '<p><i class="fas fa-comment" style="color: #4f4dff;"></i> IDを作成する<BR>';
    echo 'ニックネームを入力してください。あとから変更可能です。IDは、作成後気に入らなければ削除可能です。</p>';
    echo '<p id="createform">Nickname <input type="text" id="createnickname"> <input type="button" onclick="createid();" value="IDを作成"></p>';
    echo '<div style="color: red" id="createidresult"></div><div id="creatediddetailblock" style="display: none"><p>IDは<span id="createdid" style="font-size: 18px"></span><input type="button" onclick="createdidcopy();" id="createdidcopybtn" value="IDをコピー"></p><p>アップロード用URLは<span id="createduploadurl" style="font-size: 18px"></span><input type="button" onclick="createduploadurlcopy();" id="createdurlcopybtn" value="アップロード用URLをコピー"></p></div></div>';
    exit;
}


//div.playerprofile_playmusictop10_tableall => max-width: 400px; 
echo '<style type="text/css"><!--';
echo 'div.playerprofile_playmusictop10_tableall { display: table; table-layout: fixed; font-size: 14px; width: 100%; text-align: center; word-wrap: break-word; border-width: 0px 0px 3px 0px; border-color: #2d6495; border-style: solid; height: 30px; }';
echo 'div.playerprofile_playmusictop10_cellmname { display: table-cell; vertical-align: middle; width: 25%; border-width: 0px 3px 0px 3px; border-color: #2d6495; border-style: solid;}';
echo 'div.playerprofile_playmusictop10_cellcount { display: table-cell; vertical-align: middle; width: 75%; border-width: 0px 3px 0px 0px; border-color: #2d6495; border-style: solid; position: relative; padding-top: 4px; padding-bottom: 9px; }';
echo '--></style>';

$sqlquery = "select * from queue_data where uploadid in ( select uploadid from queue_list where userid = '".$loginid."' and uploadtimejst > now() - interval 6 day ) group by cellid order by cancelReason ASC, uploadedtime = '0000-00-00 00:00:00' DESC,uploadedtime desc,cellId ASC";


//$sqlquery = "SELECT uploadid,uploadtimejst,lastsendtimejst,queuestatus,sendcount from queue_list where userid = '".$loginid."' ORDER BY uploadtimejst DESC";
//echo $sqlquery;
unset($db_result);
if ($result = $db_session->query($sqlquery)) {
	while ($row_data = $result->fetch_assoc()) {
		$db_result[] = $row_data;
	}
	$result->free();
}

$datacount = count($db_result);
if ($datacount > 0) {
    echo '<div style="margin: 6px 10px;">';
    echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
    echo '<div class="playerprofile_playmusictop10_cellmname" style="border-width: 3px 3px 0px 3px;">キャリアおよび<BR>eNB-LCID</div>';
    echo '<div class="playerprofile_playmusictop10_cellcount" style="border-width: 3px 3px 0px 0px;">MLSへの反映が行われた日時</div>';
    echo '</div>';
    for($rec=0; $rec < count($db_result); $rec++) {
            //キャリア判定
            if ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "11") {
                $CarrierColor = "hotpink";
                $CarrierName = "Rakuten";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "53") {
                $CarrierColor = "darkblue";
                $CarrierName = "Rakuten Roaming";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "50") {
                $CarrierColor = "orange";
                $CarrierName = "au";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "51") {
                $CarrierColor = "orange";
                $CarrierName = "au VoLTE";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "54") {
                $CarrierColor = "orange";
                $CarrierName = "au 5G-SA";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "10") {
                $CarrierColor = "crimson";
                $CarrierName = "NTT docomo";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "20") {
                $CarrierColor = "dimgray";
                $CarrierName = "Softbank";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "0") {
                $CarrierColor = "tomato";
                $CarrierName = "Y!mobile";
            } elseif ($db_result[$rec]['mobileCountryCode'] == "440" and $db_result[$rec]['mobileNetworkCode'] == "5") {
                $CarrierColor = "slateblue";
                $CarrierName = "WCP";
            } else {
                $CarrierColor = "black";
                $CarrierName = "Other ".$db_result[$rec]['mobileCountryCode']."-".$db_result[$rec]['mobileNetworkCode'];
            }

            echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
            echo '<div class="playerprofile_playmusictop10_cellmname"><div style="font-size: 10px; color: white; padding: 2px; max-width: 80px; margin: 3px auto; background-color: '.$CarrierColor.'">'.$CarrierName.'</div>'.floor($db_result[$rec]['cellId']/256).'-'.($db_result[$rec]['cellId'] % 256).'</div>';
            $uploadedTime = $db_result[$rec]['uploadedTime'];

            //sendcountは実はSELECTできていないので常に0になる
            if ($uploadedTime == "0000-00-00 00:00:00") {$uploadedTime = '反映されていません (<a onclick="UploadCancel(\''.$db_result[$rec]['itemid'].'\',\''.$db_result[$rec]['cellId'].'\',\''.$db_result[$rec]['sendcount'].'\');">キャンセルする</a>)'; } else { date('Y/m/d H:i:s',$uploadedTime); }
            if ($db_result[$rec]['uploadCancel'] == "1") {
                switch ($db_result[$rec]['cancelReason']) {
                    case 1:
                        $cancelReason = "手動で";
                        break;
                    case 2:
                        $cancelReason = "除外eNB-LCIDのため";
                        break;
                    case 3:
                        $cancelReason = "ジオフェンス範囲内のため";
                        break;
                    case 4:
                        $cancelReason = "44053のため";
                        break;
                    case 5:
                        $cancelReason = "44011以外のため";
                        break;
                    default:
                        $cancelReason = "不明な理由で";
                        break;
                }
                $uploadedTime = $cancelReason.'キャンセルされました<!-- (<a onclick="UploadResume(\''.$db_result[$rec]['itemid'].'\',\''.$db_result[$rec]['cellId'].'\');">再度アップロード</a>)-->';
            }
            echo '<div class="playerprofile_playmusictop10_cellcount" id="'.$db_result[$rec]['itemid'].'">'.$uploadedTime.'</div>';
            echo '</div>';
    }
    
    echo '</div>';
} else {
    echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> MLSデータアップロードマネージャーに電測データがアップロードされていません';
    echo '</div>';
}



exit;

$sqlquery = "SELECT uploadid,uploadtimejst,lastsendtimejst,queuestatus,sendcount from queue_list where userid = '".$loginid."' ORDER BY uploadtimejst DESC";
echo $sqlquery;
unset($db_result);
if ($result = $db_session->query($sqlquery)) {
	while ($row_data = $result->fetch_assoc()) {
		$db_result[] = $row_data;
	}
	$result->free();
}

for($rec=0; $rec < count($db_result); $rec++) {
    echo '<H2 style="font-size: 16px; margin:0;padding:0;">'.$db_result[$rec]['uploadtimejst'].' にアップロードされたデータ</H2>';
    
    echo '<div style="font-size: 11px;">Upload Session#'.$db_result[$rec]['uploadid'].'</div>';
    if ($db_result[$rec]['sendcount'] !== '0') {
        echo '<div style="font-size: 11px;">Last Send: '.$db_result[$rec]['lastsendtimejst'].'</div>';
    }
    echo 'このデータは';
    switch ($db_result[$rec]['queuestatus']) {
        case 0:
            echo '内部で処理中のため、まだアップロードを試行しません。';
            break;
        case 1:
            if ($db_result[$rec]['sendcount'] == '0') {
                echo 'まもなくMLSへ初回アップロードされます。';
            } else {
                echo $db_result[$rec]['sendcount'].'回アップロードされましたが、まだ未反映のデータがあるため、1時間おきに再アップロードされます。';
            }
            break;
        case 2:
            echo $db_result[$rec]['sendcount'].'回アップロード試行され、すべてのデータが反映されました。これ以上アップロードされません。';
            break;
        case 3:
            echo $db_result[$rec]['sendcount'].'回アップロード試行され、まだ反映されていないデータがありますが、これ以上アップロードされません。';
            break;
        default:
            echo '送信されたデータが破損している可能性があります。アップロードされません。';
    }
    echo '<BR>';

    //echo '次のアップロード試行は xxxx/xx/xx xx:xx に行われます。CHECK   検出日時　eNB-LCID RSRP? 地図　未登録データ　アップロード確認日時';
    $sqlquery = "SELECT timestamp,cellId,uploadedTime from queue_data where uploadid = '".$db_result[$rec]['uploadid']."' GROUP BY cellId";
    echo $sqlquery;
    unset($db_result_queue_data);
    if ($result = $db_session->query($sqlquery)) {
	    while ($row_data = $result->fetch_assoc()) {
	    	$db_result_queue_data[] = $row_data;
	    }
	    $result->free();
    }
    echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
	echo '<div class="playerprofile_playmusictop10_cellmname" style="border-width: 1px 1px 0px 1px;">eNB-LCID</div>';
	echo '<div class="playerprofile_playmusictop10_cellcount" style="border-width: 1px 1px 0px 0px;">アップロード確認日時</div>';
    echo '</div>';
    for($rec2=0; $rec2 < count($db_result_queue_data); $rec2++) {
        echo '<div style="height: auto;" class="playerprofile_playmusictop10_tableall">';
        echo '<div class="playerprofile_playmusictop10_cellmname">'.floor($db_result_queue_data[$rec2]['cellId']/256).'-'.($db_result_queue_data[$rec2]['cellId'] % 256).'</div>';
        $uploadedTime = $db_result_queue_data[$rec2]['uploadedTime'];
        if ($uploadedTime == "0000-00-00 00:00:00") {$uploadedTime = '未アップロード'; } else { date('Y/m/d H:i:s',$uploadedTime); }
        echo '<div class="playerprofile_playmusictop10_cellcount">'.$uploadedTime.'</div>';
        echo '</div>';
    }
    echo '<P>';
    echo 'このデータのアップロードをすべてキャンセル<BR>';
    echo '</P>';
}


//MySQL接続解除
dbdisconnect($db_session);
?>