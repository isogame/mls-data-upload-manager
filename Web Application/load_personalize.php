<?php
//MySQL接続
require_once("../database_connector.php");
$db_session = dbconnect();

if ($_COOKIE['loginid'] == "") {
    echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> ログインしていないため、この機能を利用することができません。ホームタブでログイン操作を行ってください。';
    echo '</div>';
    return('');
} else {
    $sqlquery = "SELECT userid,nickname,exclude44053,only44011,speedunlimit from user_list where userid = '".escs($db_session,$_COOKIE['loginid'])."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
        if ($database_response["nickname"] !== '') {
            $loginname = $database_response["nickname"];
        } else {
            $loginname = $_COOKIE['loginid'];
        }
        echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> パーソナライズ設定をロードしました。</div>';
    } else {
        echo '<div class="eventtext" style="background-color: pink; color: #000000;"><i class="fas fa-comment" style="color: #4f4dff;"></i> このIDではユーザーデータが見つからなかったため、パーソナライズ設定は利用できません。IDを再作成してください。</div>';
        return('');
    }
    $loginid = escs($db_session,$_COOKIE['loginid']);

    //geofencinglist_csv の生成
    $sqlquery = "SELECT * from user_geofencinglist where userid = '".$loginid."'";
        unset($geofencinglist); 
        if ($result = $db_session->query($sqlquery)) {         
	        while ($row = $result->fetch_assoc()) {
		        $geofencinglist[] = $row;
	        }
	        $result->free();
        }
    $geofencinglist_count = count($geofencinglist);
    if ($geofencinglist_count > 0) {
        for($i=0;$i<$geofencinglist_count;$i++){
            $geofencinglist_csv .= $geofencinglist[$i]['lat'].",".$geofencinglist[$i]['lng'].",".$geofencinglist[$i]['radius']."\n";
        }
    }
}

//MySQL接続解除
dbdisconnect($db_session);

echo '<div style="margin: 6px 10px;">';

$nickname = $database_response["nickname"];

if ($database_response["exclude44053"] == "1") { $exclude44053checked = " checked"; }
if ($database_response["only44011"] == "1") { $only44011checked = " checked"; }
if ($database_response["speedunlimit"] == "1") { $speedunlimitchecked = " checked"; }

$HTMLContent = <<<EOM

<H2>MLSデータアップロードマネージャーのID</H2>
「MLS Upload Helper」の設定画面に入力するIDです。ほかの人に見られると、MLSデータアップロードマネージャーに他者にログインされ、アップロードした情報やこの画面で設定した情報を見られてしまいますのでご注意ください。<BR>
<span id="createdid" style="font-size: 18px">{$loginid}</span><input type="button" onclick="createdidcopy();" id="createdidcopybtn" value="IDをコピー" style="margin-left: 3px;"><BR>

<p><H3><span style="background: pink; line-height: 1; padding: 2px;">New</span> Direct Upload Integration (<a href="https://mls.js2hgw.com/wiki/?Mozilla+Location+Services+%E3%81%AE%E3%83%87%E3%83%BC%E3%82%BF%E3%83%AD%E3%82%B9%E3%83%88%E5%AF%BE%E7%AD%96">設定方法</a>)</H3>
Tower Collector から MLSデータアップロードマネージャー に直接データを送信しましょう。アップロード用URLは以下の通りです。<BR>
<span id="createduploadurl" style="font-size: 18px">https://44011.brave-hero.net/api/v1/geosubmit?key={$loginid}</span><input type="button" onclick="createduploadurlcopy();" id="createduploadurlcopybtn" value="アップロード用URLをコピー" style="margin-left: 3px;"></p>

<H2>ニックネームの設定</H2>
このIDのニックネームを設定することができます。<BR>
<form name="nicknamesettings" style="font-size: 12px; padding: 4px;">Nickname: <input type="text" id="nicknamebox" value="{$nickname}" size="35"> <input type="button" value="設定" onclick="setNickname(nicknamebox.value);"></form>
<div id="nicknamestatus"></div>

<H2>アップロードを行わない地点 (ジオフェンシング)</H2>
ここで指定した座標付近で観測されたセルのデータはMozilla Location Serviceに送信されません。<BR>
自宅付近や実家付近や勤務先付近を登録しておくと、チョット安心かもしれません。<BR>
<p>以下のマップでクリックした場所から 半径<input type="text" id="geofencing-radius" value="1200" size="5">m の範囲で記録された電測データは、次回のアップロード以降はMLSに送信されません。</p>
<div id="geofencingstatus"></div><div id="geofencinglist_csv" style="display: none;">{$geofencinglist_csv}</div>
<div id="geofencing-map" style="width: 95%; height: 400px;"></div>

<H2>アップロードを行わないeNB-LCID</H2>
ここで指定されたeNB-LCIDは、次回のアップロード以降はMLSに送信されません。<BR>
MCC <input type="text" id="excludemcc" value="440" size="4"> MNC <input type="text" id="excludemnc" value="11" size="4"> eNB-LCID <input type="text" id="excludeenblcid" value="" size="9"> <input type="button" onclick="checkCellIDFormat(document.getElementById('excludemcc').value,document.getElementById('excludemnc').value,document.getElementById('excludeenblcid').value);" value="除外設定を追加する">
<div id="excludeenbstatus"></div><div id="excludeenblist_csv" style="display: none;"></div>
<style type="text/css"><!--
div.personalizesettings_excludeenblcid_tableall { display: table; table-layout: fixed; font-size: 14px; width: 100%; text-align: center; word-wrap: break-word; border-width: 0px 0px 3px 0px; border-color: #2d6495; border-style: solid; height: 30px; max-width: 390px;}
div.personalizesettings_excludeenblcid_celldelbtn { display: table-cell; vertical-align: middle; width: 15%; border-width: 0px 3px 0px 3px; border-color: #2d6495; border-style: solid;}
div.personalizesettings_excludeenblcid_cellmcc { display: table-cell; vertical-align: middle; width: 15%; border-width: 0px 3px 0px 0px; border-color: #2d6495; border-style: solid;}
div.personalizesettings_excludeenblcid_cellmnc { display: table-cell; vertical-align: middle; width: 15%; border-width: 0px 3px 0px 0px; border-color: #2d6495; border-style: solid; }
div.personalizesettings_excludeenblcid_cellcellid { display: table-cell; vertical-align: middle; width: 55%; border-width: 0px 3px 0px 0px; border-color: #2d6495; border-style: solid; }
--></style>
<p>現在の設定
<div id="excludeenblcidlist">読み込み中...</div></p>

<H2>アップロードを行わないキャリア</H2>
<p><form name="speedsettings"><input type="checkbox" id="exclude44053" onclick="setUserSettings('exclude44053','',this.checked ? 1 : 0,'exclude44053status');"{$exclude44053checked}> 楽天モバイルのauローミング(440 53)の電測データを送信しない</form>
楽天モバイルauローミングに接続中のデータ(MCC=440,MNC=53のデータ)を送信しないようになります。<BR>
<div id="exclude44053status"></div>

<p><form name="speedsettings"><input type="checkbox" id="only44011" onclick="setUserSettings('only44011','',this.checked ? 1 : 0,'only44011status');"{$only44011checked}> 楽天モバイル関係以外の電測データを送信しない</form>
楽天モバイルMNOに接続中のデータ(MCC=440,MNC=11)のデータのみ送信するようになります。<BR>
楽天モバイル以外の電測を行い、それをMozilla Location Servicesへ送信したい場合はチェックしないでください。</p>
<div id="only44011status"></div><BR>

※現在は仕様上、MCC440/441以外のデータ送信は行われません。

<H2>移動速度の制限がかかるデータの取り扱い</H2>
50メートル/毎秒以上で移動中に観測されたセルはMozilla Location Servicesへ反映されませんが、移動速度が測定できなかったデータであることにして反映させます。<BR>
<form name="speedsettings"><input type="checkbox" id="speedunlimit" onclick="setUserSettings('speedunlimit','',this.checked ? 1 : 0,'speedunlimitstatus');"{$speedunlimitchecked}> 50メートル/毎秒以上で移動中の電測データを移動速度不明として送信する。</form>
<div id="speedunlimitstatus"></div>

<H2>MLSデータアップロードマネージャーIDの削除</H2>
直ちにすべてのデータを削除したうえで、MLSデータアップロードマネージャーのIDを削除しログアウトします。復活はできません。<BR>
この操作を行ったあとで利用を再開する場合には、もう一度IDを作成しなおしてください。<BR>
<p>実行するには、「本当に削除する」にチェックをつけてから、削除ボタンを押してください。</p>

<form name="deleteidbox"><input type="checkbox" id="deletechk">本当に削除する　<input type="button" value="削除" onclick="deleteid();"></form>
EOM;

echo $HTMLContent;
echo '</div">';
?>