-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: mysql1b.minibird.netowl.jp
-- Generation Time: 2023 年 11 月 04 日 21:23
-- サーバのバージョン： 5.7.27
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isogame_mls`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `queue_list`
--

CREATE TABLE IF NOT EXISTS `queue_list` (
  `userid` varchar(40) NOT NULL,
  `uploadid` varchar(32) NOT NULL,
  `uploadtimejst` datetime NOT NULL,
  `lastsendtimejst` datetime NOT NULL,
  `queuestatus` tinyint(4) NOT NULL,
  `sendcount` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `queue_list`
--
ALTER TABLE `queue_list`
  ADD PRIMARY KEY (`userid`,`uploadid`),
  ADD UNIQUE KEY `userid` (`userid`,`uploadid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
