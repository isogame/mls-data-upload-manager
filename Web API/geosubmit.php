<?php
    //MySQL接続
    require_once("../database_connector.php");
    $db_session = dbconnect();
    $postdata = file_get_contents("php://input");
    //$postdata = file_get_contents("testfile.txt");
    //echo $postdata;
    $mlsdata = json_decode($postdata, true);
    //$uploadid = md5(uniqid(rand(), true));
    $uploadid = md5($postdata);
    //Eecho "Upload ID:".$uploadid.PHP_EOL;
    $uploadtime = time();
    //Eecho "Upload Time:".$uploadtime.PHP_EOL;
    $uploadtimejst = (new DateTime('Asia/Tokyo'))->setTimestamp($uploadtime)->format("Y/m/d H:i:s");
    //Eecho "Upload Time JST:".$uploadtimejst.PHP_EOL;
    //Eecho "Array Count:".count($mlsdata["items"]).PHP_EOL;
    $itemcount = count($mlsdata["items"]);

    //同じハッシュになるアップロードデータが既に登録されていたら、やめる
    $sqlquery = "SELECT uploadid from queue_list where uploadid = '".$uploadid."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
        echo "This Upload Data as Exist. Goodbye or Die...".PHP_EOL;
        return('');
    }
    //Tower Collector Alternative からの送信の場合は現状、共通OpenCellId.orgのキー"pk.c1d907d5db4414943537b980adb0cf1f"が送られてくる場合がある
    $userid = escs($db_session,$_GET["key"]);
    if ($userid == "" OR $userid == "pk.c1d907d5db4414943537b980adb0cf1f") {
        //$userid = "Anonymous";
        http_response_code(400);
        echo "UserKeyNotFound;";
        return('');
    }
    //すでにこのユーザーIDのデータが登録されているか確認
    $sqlquery = "SELECT userid,point,cell,exclude44053,only44011,speedunlimit from user_list where userid = '".$userid."'";
    $result = $db_session->query($sqlquery);
    if ($database_response = $result->fetch_array(MYSQLI_ASSOC)) {
	    $pointcount = $database_response["point"];
        $cellcount = $database_response["cell"];

        $exclude44053 = $database_response["exclude44053"];
        $only44011 = $database_response["only44011"];
        $speedunlimit = $database_response["speedunlimit"];

        //Load-Exclude-eNB-List
        $sqlquery = "SELECT * from user_execludeenblist where userid = '".$userid."'";
        unset($excludeenbdata); 
        if ($result = $db_session->query($sqlquery)) {         
	        while ($row = $result->fetch_assoc()) {
		        $excludeenbdata[] = $row;
	        }
	        $result->free();
        }
        $excludeenbdata_count = count($excludeenbdata);

        //Load-Geofencing-Area
        $sqlquery = "SELECT * from user_geofencinglist where userid = '".$userid."'";
        unset($geofencinglist); 
        if ($result = $db_session->query($sqlquery)) {         
	        while ($row = $result->fetch_assoc()) {
		        $geofencinglist[] = $row;
	        }
	        $result->free();
        }
        $geofencinglist_count = count($geofencinglist);

        //Load-Upload-Options
        // $sqlquery = "SELECT * from user_settingdata where userid = '".$userid."'";
        // unset($user_settingdata); 
        // if ($result = $db_session->query($sqlquery)) {         
	    //     while ($row = $result->fetch_assoc()) {
		//         $user_settingdata[] = $row;
	    //     }
	    //     $result->free();
        // }
        // $settingvalue_count = count($user_settingdata);
        // if ($settingvalue_count > 0) {
        //     for($i=0;$i<$geofencinglist_count;$i++){
        //         $$user_settingdata[$i]['datatype'] = $user_settingdata[$i]['value'];
        //         // switch ($user_settingdata[$i]['datatype']) {
        //         //     Case "noupload44053":
        //         //         $noupload44053 = $user_settingdata[$i]['value'];
        //         //         break;
        //         //     Case "uploadonly44011":
        //         //         $uploadonly44011 = $user_settingdata[$i]['value'];
        //         //         break;
        //         //     Case "speedunlimit":
        //         //         $speedunlimit = $user_settingdata[$i]['value'];
        //         //         break;
        //         // }
        //     }
        // }

    } else {
	    //新規ユーザーの場合は、新規登録日とユーザーIDをuser_listテーブルに挿入
        //Tower Collector Alternativeの場合はこちらに入ることがあるかも
	    //$sqlquery = "INSERT INTO user_list (userid,point,cell,register_date,lastupdate_date) VALUES ('".$userid."','0','0','".date("Y-m-d")."','".date("Y-m-d")."')";
	    //if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
        //$pointcount = 0;
        //$cellcount = 0;

        //とか言っていたが、とりあえずキー不正は403にして返答
        http_response_code(403);
        echo "UserUnknown; Invalid ID Detected. Please Retry ID Create.";
        return('');
    }//新規ユーザー or 既存ユーザーおわり

    //Backup File Export
    //file_put_contents(__DIR__.'/MLSDiffRaw2/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, var_dump(getallheaders()));
    file_put_contents(__DIR__.'/QueueRaw/'.'queue_'.date('Ymd_His').'_'.$userid."_".$uploadid, file_get_contents("php://input"));

    //Insert Upload Queue General Data
    //queue_listにuploadidを書く。queuestatusは0＝データ処理中にしておく
    $sqlquery = "INSERT INTO queue_list (userid,uploadid,uploadtimejst,queuestatus) VALUES ('".$userid."','".$uploadid."','".$uploadtimejst."','0')";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
    //ArrayLoop
    $cellId="";
    for ($currentitem = 0; $currentitem <= $itemcount-1; $currentitem++) {
        $itemid = md5(uniqid(rand(), true));
        $pointcount++;
        $timestamp=escs($db_session,$mlsdata["items"][$currentitem]["timestamp"]);
        $latitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["latitude"]);
        $longitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["longitude"]);
        $accuracy=escs($db_session,$mlsdata["items"][$currentitem]["position"]["accuracy"]);
        $altitude=escs($db_session,$mlsdata["items"][$currentitem]["position"]["altitude"]);
        $heading=escs($db_session,$mlsdata["items"][$currentitem]["position"]["heading"]);
        $speed=escs($db_session,$mlsdata["items"][$currentitem]["position"]["speed"]);
        $source=escs($db_session,$mlsdata["items"][$currentitem]["position"]["source"]);
        $radioType=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["radioType"]);
        $mobileCountryCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["mobileCountryCode"]);
        $mobileNetworkCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["mobileNetworkCode"]);
        $locationAreaCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["locationAreaCode"]);
        if (!($cellId == escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["cellId"]))) {
            $cellcount++;
        }
        $cellId=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["cellId"]);
        $primaryScramblingCode=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["primaryScramblingCode"]);
        $asu=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["asu"]);
        $signalStrength=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["signalStrength"]);
        $timingAdvance=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["timingAdvance"]);
        $serving=escs($db_session,$mlsdata["items"][$currentitem]["cellTowers"][0]["serving"]);

        //この後の処理で引っかかるとこれが1になってアップロードされないitemになる予定
        $uploadCancel = 0;
        //キャンセル理由コード(0=キャンセルされていない、1=手動でのキャンセル、2=除外eNB-LCIDにヒット、3=ジオフェンシングにヒット、4=44053をアップロードしない条件にヒット、5=44011以外はアップロードしない条件にヒット)
        //1はこの一連の処理では発生せず、Web画面からユーザーが手動でキャンセル操作をした場合に記録される
        $cancelReason = 0;

        //Speed Check
        //ここのspeedがメートル毎秒だったような気がしているが要調査。50m/s（175km/h）以下にする必要がある
        if ($speedunlimit == "1" and $speed > 50) {
            $speed = "";
            //Debug
            //$cancelReason = 9;
        }

        //Exclude eNB
        if ($excludeenbdata_count > 0) {
            for($i=0;$i<$excludeenbdata_count;$i++){
                if ($excludeenbdata[$i]['MCC'] == $mobileCountryCode and $excludeenbdata[$i]['MNC'] == $mobileNetworkCode and $excludeenbdata[$i]['cellid'] == $cellId) {
                    $uploadCancel = 1;
                    $cancelReason = 2;
                }
            }
        }

        // Exclude Geofencing
        if ($geofencinglist_count > 0) {
            for($i=0;$i<$geofencinglist_count;$i++){
                if (GeoDistance($latitude,$longitude,$geofencinglist[$i]['lat'],$geofencinglist[$i]['lng'],"14") < $geofencinglist[$i]['radius']) {
                    $uploadCancel = 1;
                    $cancelReason = 3;
                }
            }
        }

        //Discard 44053 Data
        if ($exclude44053 == "1" and $mobileCountryCode == "440" and $mobileNetworkCode == "53") {
            $uploadCancel = 1;
            $cancelReason = 4;
        }

        //Only 44011 Upload!
        if ($only44011 == "1" and ($mobileCountryCode !== "440" OR $mobileNetworkCode !== "11")) {
            $uploadCancel = 1;
            $cancelReason = 5;
        }

        //Insert Queue Data
        $sqlquery = "INSERT INTO queue_data (uploadid,itemid,timestamp,latitude,longitude,accuracy,altitude,heading,speed,source,radioType,mobileCountryCode,mobileNetworkCode,locationAreaCode,cellId,primaryScramblingCode,asu,signalStrength,timingAdvance,serving,uploadCancel,cancelReason) VALUES ('".$uploadid."','".$itemid."','".$timestamp."','".$latitude."','".$longitude."','".$accuracy."','".$altitude."','".$heading."','".$speed."','".$source."','".$radioType."','".$mobileCountryCode."','".$mobileNetworkCode."','".$locationAreaCode."','".$cellId."','".$primaryScramblingCode."','".$asu."','".$signalStrength."','".$timingAdvance."','".$serving."','".$uploadCancel."','".$cancelReason."')".PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }
    }

    //Queue Data Update
    //このデータは処理してもよい状態になったので、queuestatusを1に変更
    $sqlquery = "UPDATE queue_list SET queuestatus = '1' WHERE userid='".$userid."' and uploadid='".$uploadid."'";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }

    //User Data Update
    $sqlquery = "UPDATE user_list SET point = '".$pointcount."', cell = '".$cellcount."', lastupdate_date = '".date("Y-m-d")."', upload_count = upload_count +1 WHERE userid='".$userid."'";
    if (!$result = $db_session->query($sqlquery)) { echo "error".__line__; }

    //var_dump($mlsdata);
    //MySQL接続解除
    dbdisconnect($db_session);

    echo "All Process Done!".PHP_EOL;
    //MLSのgeosubmit v2 API互換にするなら、この応答だけechoする必要がある。すでにUploadidとかいろんな情報をechoしてしまっているので、現在は厳密にはMLS互換ではない
    //Tower CollectorはHTTP 200 OKかどうかを正常に受け付けられたかどうかの判定に使っているので、それ以外の応答があっても問題ない。MLS Upload Helperはこのエンドポイントにアクセスしないし、MLS Upload HelperのAPIエンドポイントからこのエンドポイントに送るときもそこは見ていない。
    echo "{}";

    //測地線航海算法を利用した2点間の緯度経度から距離を取得するファンクション
    //outline: http://webdesign-dackel.com/2015/01/18/google-maps-api/
    //for javascript: http://hamasyou.com/blog/2010/09/07/post-2/
    //for php: http://www.cpslabo.com/blog/index.php?e=283

    //lat1,lng1= Measurement Geolocation
    //lat2,lng2= Cell Tower Geolocation
    function GeoDistance($lat1,$lng1,$lat2,$lng2,$decimal){
        // 引数　$decimal は小数点以下の桁数
        if ( (abs($lat1-$lat2) < 0.00001) && (abs($lng1-$lng2) < 0.00001) ) {
            $distance = 0;
        } else {
            $lat1 = $lat1*pi()/180;$lng1 = $lng1*pi()/180;
            $lat2 = $lat2*pi()/180;$lng2 = $lng2*pi()/180;
    
            $A = 6378140;
            $B = 6356755;
            $F = ($A-$B)/$A;
    
            $P1 = atan(($B/$A)*tan($lat1));
            $P2 = atan(($B/$A)*tan($lat2));
    
    
            $X = acos( sin($P1)*sin($P2) + cos($P1)*cos($P2)*cos($lng1-$lng2) );
            $L = ($F/8)*( (sin($X)-$X)*pow((sin($P1) + sin($P2)),2)/pow(cos($X/2) ,2) - (sin($X)-$X)*pow(sin($P1)-sin($P2),2)/pow(sin($X),2) );
    
            $distance = $A*($X+$L);
            $decimal_no=pow(10,$decimal);
            $distance = round($decimal_no*$distance)/$decimal_no;//ここでdistanceに/1000を入れるとkmになる
        }
        $format='%0.'.$decimal.'f';
        return sprintf($format,$distance);
    }
    
?>