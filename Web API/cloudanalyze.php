<?php
date_default_timezone_set('Asia/Tokyo');
echo "(CloudAnalyze) Environment Check...".PHP_EOL;
echo "Working Directory: ".getcwd().PHP_EOL;
echo "File Directory: ".(__DIR__).PHP_EOL;
chdir(__DIR__);
require_once("../database_connector.php");

echo "Starting Analyze MLS Data...".PHP_EOL;

$forcelocalfile = "false";
//trueにする場合は以下のファイルを読み込む
//$forcefilename = "MLS-diff-cell-export-2023-11-15T110000.csv.gz";

if ($forcelocalfile !== "true") {
    $rawfilename = "MLS-diff-cell-export-".gmdate('Y-m-d')."T".gmdate('H')."0000.csv.gz";
    if (!file_exists(__DIR__.'/MLSDiffRaw/'.$rawfilename)) {
        echo "Downloading ".$rawfilename."...".PHP_EOL;
        $header = stream_context_create([
            'http' => ['header' => 'Accept-Encoding: gzip'],
        ]);
        $gzipcontents = file_get_contents('https://d2koia3g127518.cloudfront.net/export/'.$rawfilename,False,$header);
    } else {
        echo "MLS Diff File ".$rawfilename." is Exists. Good bye or Die...".PHP_EOL;
        return('');
    }
}

if ($forcelocalfile == "true") {
    echo "DEBUG MODE: Continue Local File: ./MLSDiffRaw/".$forcefilename.PHP_EOL;
    $csvcontents = file_get_contents('compress.zlib://'.__DIR__.'/MLSDiffRaw/'.$forcefilename);
} elseif ($http_response_header[0] == "HTTP/1.1 200 OK") {
    echo "HTTP 200 OK; Download Completed".PHP_EOL;
    file_put_contents(__DIR__.'/MLSDiffRaw/'.$rawfilename,$gzipcontents);
    $csvcontents = file_get_contents('compress.zlib://'.__DIR__.'/MLSDiffRaw/'.$rawfilename);
} elseif ($http_response_header[0] == "HTTP/1.1 404 Not Found") {
    echo "HTTP 404 Not Found; Download Error, Abort.".PHP_EOL;
} else {
    echo "Unknown HTTP Error; Download Error, Abort.".PHP_EOL;
}

$csvcontents2 = <<<EOM
LTE,440,10,111,71862789,10,139.6820611,35.6296454,0,2,1,1692108479,1692108932,
LTE,440,10,212,71831044,13,139.7622920,35.6226365,2930,17,1,1680263860,1692108901,
LTE,440,10,212,71831045,14,139.7789358,35.6276184,22,7,1,1680711798,1692108901,
LTE,440,11,311,88098306,339,139.7178175,35.7951638,91383,4610,1,1592883903,1692111221,
LTE,440,11,311,88098307,80,139.7178623,35.7976154,93605,6640,1,1592883903,1692111214,
LTE,440,11,311,88098563,274,139.6885856,35.8297342,88294,8914,1,1592883903,1692111209,
LTE,440,11,311,88098823,129,139.6813533,35.8364648,87745,8339,1,1592883903,1692111209,
LTE,440,11,311,88098826,134,139.6858674,35.8337098,87932,1215,1,1619259439,1692111209,
LTE,440,11,311,88098828,291,139.6848115,35.8338808,88860,6441,1,1619259439,1692111209,
EOM;

//MySQL接続
$db_session = dbconnect();
echo "Database Connected.".PHP_EOL;

//$matchresult = preg_match_all("/^....?,44[0,1],.*?/ms", $csvcontents, $matches);

//v1.1
//$matchresult = preg_match_all("/^....?,(?P<MCC>44[0-1]),(?P<MNC>.*?),.*?,(?P<cellId>.*?),.*?,.*?,.*?,.*?,.*?,.*?,.*?,(?P<updateTime>.*?),/m", $csvcontents, $matches);

//v1.2
$matchresult = preg_match_all("/^(?P<Radio>....?),(?P<MCC>44[0-1]),(?P<MNC>.*?),(?P<Area>.*?),(?P<cellId>.*?),(?P<Unit>.*?),(?P<Lon>.*?),(?P<Lat>.*?),(?P<Ranges>.*?),(?P<Samples>.*?),(?P<Changeable>.*?),(?P<Created>.*?),(?P<updateTime>.*?),(?P<averageSignal>.*?)/m", $csvcontents, $matches);

echo "MCC=440 or 441 Matches: ".$matchresult.PHP_EOL;

if ($matchresult > 0) {
    echo "match: ".$matchresult.PHP_EOL;
    for($i=0;$i<$matchresult;$i++) {
        //var_dump($matches['cellid'][$i]);
        
        $currentradio = $matches['Radio'][$i];
        $currentarea = $matches['Area'][$i];
        $currentunit = $matches['Unit'][$i];
        $currentlon = $matches['Lon'][$i];
        $currentlat = $matches['Lat'][$i];
        $currentrange = $matches['Range'][$i];
        $currentSamples = $matches['Samples'][$i];
        $currentchangeable = $matches['Changeable'][$i];
        $currentCreated = $matches['Created'][$i];
        $currentupdateTime = $matches['updateTime'][$i];
        $currentavailablesignal = $matches['averageSignal'][$i];




        $currentmcc = $matches['MCC'][$i];
        $currentmnc = $matches['MNC'][$i];

        $currentcellid = $matches['cellId'][$i];
        $currentenb = floor($currentcellid / 256);
        $currentlcid = $currentcellid % 256;
        echo "MCC: ".$currentmcc." MNC: ".$currentmnc." eNB-LCID: ".$currentenb."-".$currentlcid.PHP_EOL;
        
        //updateTimeをDatetimeにする
        $sqlquery = "UPDATE queue_data SET uploadedTime = '".date('Y/m/d H:i:s',$matches['updateTime'][$i])."' WHERE mobileCountryCode='".$matches['MCC'][$i]."' and mobileNetworkCode='".$matches['MNC'][$i]."' and cellId='".$matches['cellId'][$i]."'";
        echo $sqlquery.PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "error"; }

        //MLSfullに登録する
        //INSERT INTO users (id, name, email) VALUES (1, 'John Doe', 'john@example.com') ON DUPLICATE KEY UPDATE name = VALUES(name), email = VALUES(email);
        $sqlquery = "INSERT INTO MLSfull (radio, mcc, net, area, cell, unit, lon, lat, ranges, samples, changeable, created, updated, averageSignal) VALUES ('{$matches['Radio'][$i]}', '{$matches['MCC'][$i]}', '{$matches['MNC'][$i]}', '{$matches['Area'][$i]}', '{$matches['cellId'][$i]}', '{$matches['Unit'][$i]}', '{$matches['Lon'][$i]}', '{$matches['Lat'][$i]}', '{$matches['Ranges'][$i]}', '{$matches['Samples'][$i]}', '{$matches['Changeable'][$i]}', '{$matches['Created'][$i]}', '{$matches['updateTime'][$i]}', '{$matches['averageSignal'][$i]}') ON DUPLICATE KEY UPDATE radio = VALUES(radio), mcc = VALUES(mcc), net = VALUES(net), area = VALUES(area), cell = VALUES(cell)";
        echo $sqlquery.PHP_EOL;

        //SET uploadedTime = '".date('Y/m/d H:i:s',$matches['updateTime'][$i])."' WHERE mobileCountryCode='".$matches['MCC'][$i]."' and mobileNetworkCode='".$matches['MNC'][$i]."' and cellId='".$matches['cellId'][$i]."'";
        //echo $sqlquery.PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    }
}

echo "Finished Analyze MLS Data.".PHP_EOL;

//MLS送信キューのデータ削除
echo "Starting Cleanup Progress...".PHP_EOL;
$db_session->begin_transaction();
try {
    // queue_dataから1カ月以上前のデータを削除（すべてのデータがアップロードされている場合のみ）
    $stmt1 = $db_session->prepare("DELETE FROM queue_data WHERE uploadid IN ( SELECT uploadid FROM (SELECT uploadid FROM queue_list WHERE queuestatus = '2' and uploadtimejst < (NOW() - INTERVAL 1 MONTH)) as limitresult )");
    $stmt1->execute();
    $stmt1->close();
    echo "Clean queue_data.".PHP_EOL;

    // queue_listから1カ月以上前のデータを削除（すべてのデータがアップロードされている場合のみ）
    $stmt2 = $db_session->prepare("DELETE FROM queue_list WHERE queuestatus = '2' and uploadtimejst < (NOW() - INTERVAL 1 MONTH)");
    $stmt2->execute();
    $stmt2->close();
    echo "Clean queue_list.".PHP_EOL;

    // トランザクションをコミット
    $db_session->commit();
    echo "Cleanup COMMIT.".PHP_EOL;

} catch (mysqli_sql_exception $exception) {
    // エラーがあった場合はロールバック
    $db_session->rollback();
    echo "Cleanup Rollback....".PHP_EOL;
    throw $exception;
}
echo "Finished Cleanup.".PHP_EOL;

//MySQL接続解除
dbdisconnect($db_session);
?>