<?php
date_default_timezone_set('Asia/Tokyo');
echo "(CloudSend) Environment Check...".PHP_EOL;
echo "Working Directory: ".getcwd().PHP_EOL;
echo "File Directory: ".(__DIR__).PHP_EOL;
chdir(__DIR__);
require_once("../database_connector.php");

echo "Starting Analyze MLS Data...".PHP_EOL;

require_once("cloudanalyze.php");

function send_to_mls($senddata) {
    $url = "https://location.services.mozilla.com/v2/geosubmit";
    $context = array(
        'http' => array(
            'method' => 'POST',
            'header' => implode("\r\n", array(
                        'Content-Type: application/json; charset=utf8',
                        'User-Agent: MLS Data Upload Manager',
                        'Content-Length: '.strlen($senddata))),
            'content' => $senddata
        )
    );
    echo "Starting To MLS Send Process...".PHP_EOL;
    $response = file_get_contents($url, false, stream_context_create($context));
    echo "MLS Response: ".$http_response_header[0].": ".$response.PHP_EOL;
}

function load_queue_data($uploadid) {
    $db_session = dbconnect();
    echo "load_queue_data(".$uploadid.") Started".PHP_EOL;
    $sqlquery = "SELECT * from queue_data where uploadid = '".$uploadid."' and uploadedTime = '0000-00-00 00:00:00' and uploadCancel = '0'";
    echo $sqlquery.PHP_EOL;
    unset($db_data); 
    if ($result = $db_session->query($sqlquery)) {         
	    while ($row = $result->fetch_assoc()) {
		$db_data[] = $row;
	    }
	    $result->free();
    }
    
    $count = count($db_data);
    echo "Queue ".$uploadid." Items Count: ".$count.PHP_EOL;
    if ($count > 0) {
        echo "Upload Item Available, Generating JSON...".PHP_EOL;
        //DBのカラム名とJSONの項目名の対応付け。現在はすべての項目でイコールなのでこのように表現せず'latitude', 'longitude', 'accuracy'と書いてしまってもよい
        $PositionArrayFields = [
            'latitude' => 'latitude',
            'longitude' => 'longitude',
            'accuracy' => 'accuracy',
            'altitude' => 'altitude',
            'heading' => 'heading',
            'speed' => 'speed',
            'source' => 'source'
        ];
        $CellTowersArrayFields = [
            'radioType' => 'radioType',
            'mobileCountryCode' => 'mobileCountryCode',
            'mobileNetworkCode' => 'mobileNetworkCode',
            'locationAreaCode' => 'locationAreaCode',
            'cellId' => 'cellId',
            'primaryScramblingCode' => 'primaryScramblingCode',
            'asu' => 'asu',
            'signalStrength' => 'signalStrength',
            'timingAdvance' => 'timingAdvance',
            'serving' => 'serving'
        ];
        for($i=0;$i<$count;$i++){
            $PositionArray=array();
            foreach ($PositionArrayFields as $ArrayName => $db_Column) {
                if($db_data[$i][$db_Column] !== "") {
                    $PositionArray=array_merge($PositionArray, array($ArrayName=>$db_data[$i][$db_Column]));
                }
            }
            $CellTowersArray=array();
            foreach ($CellTowersArrayFields as $ArrayName => $db_Column) {
                if($db_data[$i][$db_Column] !== "") {
                    $CellTowersArray=array_merge($CellTowersArray, array($ArrayName=>$db_data[$i][$db_Column]));
                }
            }
            $ItemArray=array('timestamp'=>$db_data[$i]['timestamp'],'position'=>$PositionArray,'cellTowers'=>[$CellTowersArray]);
            $ItemsArray[]=$ItemArray;
        }
        $senddata_rawjson=json_encode(array('items'=>$ItemsArray),JSON_PRETTY_PRINT);
    
        //sourceとradioType以外のデータからダブルクオーテーションを削除したい人生だった　その1
        //$replace_regex = '/("(?!.*(source|radioType)).+? )(")(.*)(")/';
        //$senddata_formatting = str_replace('"', '', $senddata_rawjson);
    
        //sourceとradioType以外のデータからダブルクオーテーションを削除したい人生だった　その2
        //$search_pattern = array ('/("source": )"(.*)"/','/("radioType": )"(.*)"/');
        //$search_pattern = '/?!.*("source": |"radioType": )(.*?)/';

        //人生うまくいかないのでこう
        $search_pattern = array ('/("timestamp": )"(.*)"/','/("latitude": )"(.*)"/','/("longitude": )"(.*)"/','/("accuracy": )"(.*)"/','/("altitude": )"(.*)"/','/("heading": )"(.*)"/','/("speed": )"(.*)"/','/("mobileCountryCode": )"(.*)"/','/("mobileNetworkCode": )"(.*)"/','/("locationAreaCode": )"(.*)"/','/("cellId": )"(.*)"/','/("primaryScramblingCode": )"(.*)"/','/("asu": )"(.*)"/','/("signalStrength": )"(.*)"/','/("timingAdvance": )"(.*)"/','/("serving": )"(.*)"/');
        $senddata_formatting = preg_replace($search_pattern, '$1'."$2".'', $senddata_rawjson);
        echo $senddata_formatting.PHP_EOL;

        send_to_mls($senddata_formatting);
    } else {
        echo "Upload Item Not Available, Skipped.".PHP_EOL;
        echo "Upload ID Disabling...".PHP_EOL;
        //ここではuseridを見ずにuploadidを無効化しているが、果たして.....
        $sqlquery = "UPDATE queue_list SET queuestatus = '2' WHERE uploadid='".$uploadid."'";
        if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    }
    dbdisconnect($db_session);
}

function load_queue_list() {
    $db_session = dbconnect();
    echo "load_queue_list() Started".PHP_EOL;
    $sqlquery = "SELECT * from queue_list where uploadtimejst >= date_add(now(), interval -30 day) and queuestatus = '1' order by uploadtimejst desc";
    //echo $sqlquery;

    unset($db_data); 
    if ($result = $db_session->query($sqlquery)) {         
      while ($row = $result->fetch_assoc()) {
         $db_data[] = $row;
     }
    $result->free();
    }

    $count = count($db_data);
    echo "Queue Count: ".$count.PHP_EOL;
    for($i=0;$i<$count;$i++){
        $current_uploadid = $db_data[$i]['uploadid'];
        if ($db_data[$i]['sendcount'] <= 30) {
            echo "Current Processing Upload ID: ".$current_uploadid.PHP_EOL;
            load_queue_data($current_uploadid);
            $sqlquery = "UPDATE queue_list SET sendcount = sendcount + 1 , lastsendtimejst = NOW() WHERE uploadid = '".$db_data[$i]['uploadid']."'";
            echo $sqlquery.PHP_EOL;
        } else {
            echo "Upload ID ".$current_uploadid." is Upload Count exceeded! Data Discarding...".PHP_EOL;
            //ここでもUploadidしかみていないが、はたして・・・
            $sqlquery = "UPDATE queue_list SET queuestatus = '3' WHERE uploadid='".$current_uploadid."'";
            if (!$result = $db_session->query($sqlquery)) { echo "error"; }
        }

        echo "Upload ID: ".$current_uploadid." Process Completed.".PHP_EOL.PHP_EOL;
        if (!$result = $db_session->query($sqlquery)) { echo "error"; }
    }
    dbdisconnect($db_session);
}

echo "Starting Send MLS Data...".PHP_EOL;
load_queue_list();

echo "All Process Completed!".PHP_EOL;
?>