package net.brave_hero.isogame.mlsuploadhelper;

import android.content.DialogInterface;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import net.brave_hero.isogame.mlsuploadhelper.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;

import android.widget.TextView;
import android.widget.Toast;





public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        appBarConfiguration = new AppBarConfiguration.Builder(navController.getGraph()).build();
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);

        TextView abouttext = (TextView)findViewById(R.id.abouttext);
        String src = getString(R.string.mlsuploadmgr_about);
        abouttext.setText(Html.fromHtml(src));
        abouttext.setMovementMethod(LinkMovementMethod.getInstance());


        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("About: MLS Upload Helper")
                    .setMessage("(c) 2023 isogame (isogame@sika3.com)\nMLS Upload Helper version 1.0 Public Beta (20231024 Build)")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OKボタンをクリックしたときの処理
                        }
                    })
//                    .setNegativeButton("LICENSE", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            new AlertDialog.Builder(MainActivity.this)
//                                    .setTitle("LICENSE")
//                                    .setMessage("Mozilla Public License 2.0 (予定)\n\nCovered Software is provided under this License on an “as is” basis, without warranty of any kind, either expressed, implied, or statutory, including, without limitation, warranties that the Covered Software is free of defects, merchantable, fit for a particular purpose or non-infringing. The entire risk as to the quality and performance of the Covered Software is with You. Should any Covered Software prove defective in any respect, You (not any Contributor) assume the cost of any necessary servicing, repair, or correction. This disclaimer of warranty constitutes an essential part of this License. No use of any Covered Software is authorized under this License except under this disclaimer.")
//                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            // OKボタンをクリックしたときの処理
//                                        }
//                                    })
//                                    .show();
//                        }
//                    })
                    //.setNegativeButton("キャンセル", null)
                    .show();

            return true;
        } else if (id == R.id.show_privacy_policy) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("プライバシーポリシー")
                    .setMessage("MLS Upload Helper では、以下のような情報を取り扱います。\n" +
                            "\n" +
                            "・インターネットへのアクセス\n" +
                            "MLS Upload Helperは、Tower Collectorと「MLSデータアップロードマネージャー」との間で情報の橋渡しをするアプリケーションですので\n" +
                            "「MLSデータアップロードマネージャー」のサーバーとの通信が発生します。\n" +
                            "ここで「Tower Collector」からエクスポートしたデータを送信します。データは送信前に一部加工される場合があります。\n" +
                            "\n" +
                            "・MLS Upload Helperから送信するファイルには位置情報が含まれます\n" +
                            "MLS Upload Helperが直接アクセス権を要求しているわけではありませんが、Tower Collectorからエクスポートし\n" +
                            "あなたが「MLS Upload Helper」アプリケーションと共有することを選択したファイルには「位置情報」データが含まれます。\n" +
                            "MLS Upload HelperにTower Collectorからエクスポートしたデータを共有することで、データがこのサイトの管理人から見える場所に渡ってしまうことに注意してください。\n" +
                            "これには Tower Collector で基地局データをマッピングしている間のすべての時刻データ、その時刻に受信していた基地局のセル情報、GPS位置情報データが含まれます。\n" +
                            "この点に同意できない場合は、このシステムを利用することができませんので、ご了承ください（仕組み上仕方ないことです）\n" +
                            "また、この「MLSデータアップロードマネージャー」で発行する「MLSデータアップロードマネージャーID」は他人に知られないようにしてください。知られると、あなたの電測データを閲覧される可能性があります。\n" +
                            "\n" +
                            "MLS Upload Helperから「MLSデータアップロードマネージャー」へ送信したデータの削除について\n" +
                            "MLS Upload Helper の設定画面で「MLSデータアップロードマネージャー」のIDを入力する箇所がございます。\n" +
                            "MLS Upload Helper から送信した全てのJSONファイルは「MLSデータアップロードマネージャー」に蓄積され、処理されます。\n" +
                            "基本的には処理が終わった後、30日でデータは自動的に削除されますが、すぐに削除したい場合、または、意図して利用終了を宣言したい場合はIDを削除する必要があります。\n" +
                            "\n" +
                            "削除方法：「MLSデータアップロードマネージャー」で該当のIDでログインし「パーソナル設定」画面に遷移し、一番下までスクロールしていただくと\n" +
                            "「IDの削除」が行える画面が表示されますので、そちらから「ID削除」を実行することで、そのID自体、そのIDで関連づけられたデータをすべて削除できます。")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // OKボタンをクリックしたときの処理
                        }
                    })
                    .show();

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

}

